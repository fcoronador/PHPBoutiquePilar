<?php

use Illuminate\Support\Facades\Route;


Route::get('/','ControlInicio@index')->name('inicio');

Route::get('/index/{cat}','ControlInicio@subcategorias')->name('nuevosubcat');


Route::view('/Catalogo','Catalogo')->name('catalogo'); 
Route::get('/Catalogo/{subcat}','ControlSubcategoria@show')->name('subcatalogo'); 

Route::view('/Producto','Producto')->name('producto');
Route::get('/Producto/{prod}','ControlProducto@show')->name('productos');



Route::view('/admin','admin')->name('admin')->middleware('auth');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('registro', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('registro', 'Auth\RegisterController@register');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

// Rutas de administración
Route::get('admin/','ControlCatalogo@index')->name('admin');
Route::get('admin/vercatalogo/','ControlCatalogo@index2')->name('Vercatalogo');
Route::post('admin/vercatalogo/','ControlCatalogo@store')->name('Guardarcatalogo');

//Rutas producto

Route::get('admin/Producto/{prod}/editar','ControlProducto@edit')->name('Actualizarproducto');
Route::patch('admin/Producto/{prod}/actualizar','ControlProducto@update')->name('Updateproducto');
Route::patch('admin/Producto/{prod}/eliminar','ControlProducto@delete')->name('Borrarproducto');
/* Route::delete('admin/Producto/{prod}/eliminar','ControlProducto@destroy')->name('Borrarproducto'); */

//Rutas Subcategoria

Route::get('admin/Subcategoria/{id}/editar','ControlSubcategoria@edit')->name('Actualizarsubcategoria');
Route::patch('admin/Subcategoria/{id}/actualizar','ControlSubcategoria@update')->name('Updatesubcategoria');
Route::patch('admin/Subcategoria/{id}/eliminar','ControlSubcategoria@delete')->name('Borrarsubcategoria'); 
/* Route::delete('admin/Subcategoria/{id}/eliminar','ControlSubcategoria@destroy')->name('Borrarsubcategoria'); */

//Rutas Categoria

Route::get('admin/Categoria/{id}/editar','ControlCategoria@edit')->name('Actualizarcategoria');
Route::patch('admin/Categoria/{id}/actualizar','ControlCategoria@update')->name('Updatecategoria');
Route::patch('admin/Categoria/{id}/eliminar','ControlCategoria@delete')->name('Borrarcategoria');
/* Route::delete('admin/Categoria/{id}/eliminar','ControlCategoria@destroy')->name('Borrarcategoria'); */



