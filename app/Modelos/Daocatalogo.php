<?php

namespace App\Modelos;

use Illuminate\Support\Facades\DB;

class Daocatalogo{

    private $query ='select distinct cate.*,subcate.*,produ.*
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)';
    private $listacatalogo;

    public function __construct()
    {  }

    public function getCatalogo(){
        
        $this->listacatalogo=DB::select($this->query);
        return $this->listacatalogo;
    }


}