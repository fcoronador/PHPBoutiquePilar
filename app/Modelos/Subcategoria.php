<?php

namespace App\Modelos;

use App\Modelos\Consultas;
use App\Modelos\Daosubcategoria;
use App\Modelos\Daocatalogo;
use App\Modelos\Producto;


class Subcategoria

{
    public $id_subcategoria, $id_categoria, $nombre_subcategoria, $descripcion;
    public $productos = [];
    public $prod = [];
    private $dao;

    public function __construct()
    {
        $this->dao = new Daosubcategoria();
    }

    public function CrearSubcategoria($s)
    {

        $this->AddProductos();

        foreach ($s as $item) {
            $this->id_subcategoria = $s['id_subcategoria'];
            $this->id_categoria = $s['id_categoria'];
            $this->nombre_subcategoria = $s['nombre_subcategoria'];
            $this->descripcion = $s['descripcion'];
        }
        foreach ($this->productos as $i) {
            if ($s['id_subcategoria'] == $i->id_subcategoria) {
                $this->prod[] = $i;
            }
        }
    }

    public function listaSubcategoria()
    {

        return $this->dao->getSubcategorias();
    }

    public function AddProductos()
    {

        $data = new Catalogo();
        $catalogo = $data->ConsultarCatalogo();

        foreach ($catalogo as $item) {
            $p['id_categoria'] = $item->id_categoria;
            $p['id_subcategoria'] = $item->id_subcategoria;
            $p['id_producto'] = $item->id_producto;
            $p['nombre_producto'] = $item->nombre_producto;
            $p['descripcion'] = $item->descripcion;
            $p['valor_unitario'] = $item->valor_unitario;
            $p['precio_venta'] = $item->precio_venta;
            $p['referencia'] = $item->referencia;
            $p['material'] = $item->material;
            $p['medida'] = $item->medida;
            $p['img'] = $item->img;
            $nuevo_producto = new Producto();
            $nuevo_producto->CrearProducto($p);
            $this->productos[] = $nuevo_producto;
        }
    }


    public function CalcularSubcategorias()
    {
        return $this->dao->getSubcategoriasenUso();
    }


    public function GuardarSubcategoria($s)
    {
        return $this->dao->setSubcategoria($s);
    }

    public function SeleccionarSubcategoria($s)
    {
        
        $subcat=$this->dao->getSubcategoria($s);
        return $subcat;   
    }

    public function SelectSubcategoria($s)
    {
        
        $subcat=$this->dao->getSub($s);
        return $subcat;   
    }

    public function ActualizarSubcategoria($s)
    {

        $this->dao->updateSubcategoria($s); 
    }

    public function EliminarSubcategoria($id){

        $this->dao->deleteSubcategoria($id);
    }
}
