<?php

namespace App\Modelos;

use App\Modelos\Daocatalogo;
use App\Modelos\Categoria;


class Catalogo 

{

    public $categorias=[];


    public function __construct()
    {
         
    }
    
    public function AddCategorias(){

        $categ=new Categoria();
        $data= $categ->listaCategoria()->getCategorias();

        foreach($data as $item){

            $c['id_categoria']=$item->id_categoria;
            $c['nombre_categoria']=$item->nombre_categoria;
            $c['descripcion']=$item->descripcion;
            $nueva_categoria=new Categoria();
            $nueva_categoria->CrearCategoria($c);
            $this->categorias[]=$nueva_categoria;
        }
        return $this->categorias;
    }

    public function ConsultarCatalogo(){
        $cata=new Daocatalogo();
        return $cata->getCatalogo();
    }
    
}