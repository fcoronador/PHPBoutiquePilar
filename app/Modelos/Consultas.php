<?php

namespace App\Modelos;

use Illuminate\Support\Facades\DB;

class Consultas

{
    private $consultabase ='select distinct cate.*,subcate.*,produ.*
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)';
    
    /* consulta para obtener los productos de un subcatalogo especificico */
    private $consulta2='select * from (
    select distinct cate.*,subcate.*,produ.*
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria) ) as global where nombre_subcategoria=:id';

    /*consulta para obtener subcatalogos con productos  */

    private $consulta3="select distinct subcate.*,subcate.id_categoria
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)";

    private $Consulta_productos='select distinct cate.id_categoria,cate.nombre_categoria,subcate.id_subcategoria,subcate.nombre_subcategoria,produ.*
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)';

    private $Consulta_subcategoria='select distinct cate.id_categoria,cate.nombre_categoria,subcate.id_subcategoria,subcate.nombre_subcategoria
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)';

    private $subcatalogo1;
    private $catalogos;
    private $subcatalogos;
    private $producto;
    private $productos;
    private $todos;
    private $subcategorias;


    public function __construct()
    {  }
    
    public function getTodos(){
        $this->todos=DB::select($this->consultabase);
        return $this->todos;
    }


    public function getSubcatalogo1(){
        $this->subcatalogo1=DB::select($this->consulta3);
        return $this->subcatalogo1;
    }
    
    public function getCatalogo(){
        $this->catalogos=DB::select('SELECT * FROM categoria ORDER BY nombre_categoria');
        return $this->catalogos;
    }
  
    public function getSubcatalogo($id){
        $this->subcatalogos= DB::select($this->consulta2, ['id'=>$id]);
        return $this->subcatalogos;
    }

    public function getProducto($id){
        $this->producto=DB::table('producto')->where('referencia',$id)->get();
        return $this->producto;
    }

    public function getProductos(){
        $this->productos=DB::select($this->Consulta_productos);
        return $this->productos;
    }

    public function getSubcategorias(){
        $this->subcategorias=DB::select($this->Consulta_subcategoria);
        return $this->subcategorias;
    }
}