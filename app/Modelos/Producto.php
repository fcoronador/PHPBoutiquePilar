<?php

namespace App\Modelos;

use App\Modelos\Daoproducto;

class Producto

{
    public $id_categoria, $id_subcategoria, $id_producto,
        $nombre_producto, $descripcion, $valor_unitario,
        $precio_venta, $referencia, $material, $medida,
        $img;
    private $dao;

    public function __construct()
    {
        $this->dao = new Daoproducto();
    }

    public function CrearProducto($p)
    {

        foreach ($p as $i) {
            $this->id_categoria = $p['id_categoria'];
            $this->id_subcategoria = $p['id_subcategoria'];
            $this->id_producto = $p['id_producto'];
            $this->nombre_producto = $p['nombre_producto'];
            $this->descripcion = $p['descripcion'];
            $this->valor_unitario = $p['valor_unitario'];
            $this->precio_venta = $p['precio_venta'];
            $this->referencia = $p['referencia'];
            $this->material = $p['material'];
            $this->medida = $p['medida'];
            $this->img = $p['img'];
        }
    }

    public function CalcularProductos()
    {
        $producto = $this->dao;
        return $producto;
    }

    public function SeleccionarProducto($id)
    {

        $producto = Daoproducto::getProducto($id);
        return $producto;
    }

    public function AlbumProducto($id)
    {
        return $album = Daoproducto::getAlbumProducto($id);
    }

    public function Guardarproducto($p)
    {

        Daoproducto::setProducto($p);
    }

    public function Modificarproducto($p)
    {

        Daoproducto::updateProducto($p);
    }

    public function Borrarproducto($id)
    {

        Daoproducto::deleteProducto($id);
    }

    public function Album($imagenes){
        Daoproducto::setAlbum($imagenes);
    }
    public function UAlbum($imagenes){
        Daoproducto::updateAlbum($imagenes);
    }
    public function SeleccionarAlbum(){
        return Daoproducto::getAlbum(); 
    }
}
