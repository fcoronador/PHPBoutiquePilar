<?php

namespace App\Modelos;

use Illuminate\Support\Facades\DB;

class Daosubcategoria
{

    private $query = 'select * from sub_categoria';
    private $Consulta_subcategoria = 'select distinct cate.id_categoria,cate.nombre_categoria,subcate.id_subcategoria,subcate.nombre_subcategoria, subcate.visible, subcate.img as img
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria) order by subcate.nombre_subcategoria';

    private $consulta2 = 'select * from (
        select distinct cate.*,subcate.*,produ.*
        FROM sub_categoria as subcate
        INNER JOIN 
        categoria AS cate ON 
        (cate.id_categoria=subcate.id_categoria)
        INNER JOIN
        producto AS produ ON
        (produ.id_subcategoria=subcate.id_subcategoria) ) as global where nombre_subcategoria=:id';

    private $listasubcategorias;

    public function __construct()
    {
    }

    public function getSubcategorias()
    {

        $this->listasubcategorias = DB::select($this->query);
        return $this->listasubcategorias;
    }

    public function setSubcategoria($s)
    {

        DB::insert('insert into sub_categoria (id_categoria, nombre_subcategoria,descripcion,img, visible) 
            values (:id_categoria, :nombre_subcategoria , :descripcion, :img, :visible)', $s);
    }

    public function getSubcategoriasenUso()
    {
        $this->subcategorias = DB::select($this->Consulta_subcategoria);
        return $this->subcategorias;
    }

    public function getSubcategoria($id)
    {
        
        $subcatalogos = DB::select( $this->consulta2, ['id' => $id]);
        
        return $subcatalogos;  
    }

    public function getSub($id)
    {
        
        $subcatalogos = DB::select('select * from sub_categoria where id_subcategoria=:id', ['id' => $id]);
        
        return $subcatalogos;  
    }

    public function updateSubcategoria($s)
    {

        DB::table('sub_categoria')
            ->where('id_subcategoria', $s['id_subcategoria'])
            ->update($s);
    }

    public function deleteSubcategoria($id)
    {

        DB::table('producto')->where('id_subcategoria', '=', $id)->delete();
        DB::table('sub_categoria')->where('id_subcategoria', '=', $id)->delete();
    }
}
