<?php

namespace App\Modelos;

use Illuminate\Support\Facades\DB;

class Daoproducto
{

    private $query = 'select * from producto';
    private $Consulta_productos = 'select distinct cate.id_categoria,cate.nombre_categoria,subcate.id_subcategoria,subcate.nombre_subcategoria,produ.*
    FROM sub_categoria as subcate
    INNER JOIN 
    categoria AS cate ON 
    (cate.id_categoria=subcate.id_categoria)
    INNER JOIN
    producto AS produ ON
    (produ.id_subcategoria=subcate.id_subcategoria)';
    private $listaProductos, $productos;

    public function __construct()
    {
    }

    public function getProductos()
    {

        $this->listaProductos = DB::select($this->query);
        return $this->listaProductos;
    }

    public function getProductosenUso()
    {
        $this->productos = DB::select($this->Consulta_productos);
        return $this->productos;
    }

    public static function getProducto($id)
    {
        $producto = DB::table('producto')->where('id_producto', $id)->get();
        return $producto;
    }

    public static function getAlbumProducto($id)
    {
        $album = DB::table('imagenes')->where('id_producto', $id)->get();
        return $album;
    }

    public static function setProducto($p)
    {

        DB::insert('insert into producto (id_subcategoria, descripcion, referencia, nombre_producto, material,precio_venta, valor_unitario, medida, visible ,img , comprar)values (:id_subcategoria, :descripcion, :referencia,:nombre_producto, :material, :precio_venta, :valor_unitario, :medida, :visible ,:img, :comprar)', $p);

        $producto = DB::select("SELECT currval('producto_id_producto_seq')");
        $id = [];
        foreach ($producto as $item) {
            $id['id_producto'] = $item->currval;
        }

        DB::insert('insert into imagenes (id_producto)values(:id_producto)',$id);

    }

    public static function updateProducto($p)
    {

        DB::table('producto')
            ->where('id_producto', $p['id_producto'])
            ->update($p);
    }

    public static function deleteProducto($id)
    {

        DB::table('producto')->where('id_producto', '=', $id)->delete();
    }

    public static function setAlbum($imagenes)
    {
        $producto = DB::select("SELECT currval('producto_id_producto_seq')");
        
        foreach ($producto as $item) {
            $imagenes['id_producto'] = $item->currval;
        }
        DB::table('imagenes')
            ->where('id_producto', $imagenes['id_producto'])
            ->update($imagenes);

        /* DB::insert('insert into imagenes ( img2,img3,img4,img5,img6,img7,img8 , id_producto)values(:img2,   :img3, :img4, :img5, :img6, :img7, :img8, :id_producto)', $imagenes); */
    }

    public static function updateAlbum($imagenes)
    {
       
        DB::table('imagenes')
            ->where('id_producto', $imagenes['id_producto'])
            ->update($imagenes);
    }

    public static function getAlbum() 
    {
        $album = DB::select('select p.id_producto,i.* from (producto as p join imagenes as i on p.id_producto=i.id_producto)');
        return $album; 
    }

}
