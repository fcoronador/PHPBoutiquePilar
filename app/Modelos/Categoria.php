<?php

namespace App\Modelos;

use App\Modelos\Consultas;
use App\Modelos\Daocategoria;
use App\Modelos\Subcategoria;

class Categoria

{
    public $id_categoria, $nombre_categoria, $descripcion;
    public $subcategorias = [];
    public $subcat = [];
    private $dao;

    public function __construct()
    {
        $this->dao = new Daocategoria();
    }

    public function CrearCategoria($c)
    {

        $this->AddSubcategorias();

        foreach ($c as $item) {
            $this->id_categoria = $c['id_categoria'];
            $this->nombre_categoria = $c['nombre_categoria'];
            $this->descripcion = $c['descripcion'];
        }
        foreach ($this->subcategorias as $i) {
            if ($c['id_categoria'] == $i->id_categoria) {
                $this->subcat[] = $i;
            }
        }
    }

    public function AddSubcategorias()
    {

        $subcat = new Subcategoria();
        $subcat->AddProductos();
        $data = $subcat->listaSubcategoria();

        foreach ($data as $item) {

            $s['id_subcategoria'] = $item->id_subcategoria;
            $s['id_categoria'] = $item->id_categoria;
            $s['nombre_subcategoria'] = $item->nombre_subcategoria;
            $s['descripcion'] = $item->descripcion;
            $nueva_subcategoria = new Subcategoria();
            $nueva_subcategoria->CrearSubcategoria($s);
            $this->subcategorias[] = $nueva_subcategoria;
        }
    }


    public function listaCategoria()
    {
        $categoria = new Daocategoria();
        return $categoria;
    }


    public function CalcularCategorias()
    {
        $consultas = new Consultas();
        return $categoria = $consultas->getCatalogo();
    }

    public function GuardarCategoria($c)
    {
        $this->dao->setCategoria($c);
    }

    public function SeleccionarCategoria($id)
    {
        return $this->dao->getCategoria($id);
    }

    public function ActualizarCategoria($c)
    {
        $this->dao->updateCategoria($c); 
    }

    public function BorrarCategoria($id)
    {

        $this->dao->deleteCategoria($id);
    }
}
