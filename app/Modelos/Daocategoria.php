<?php

namespace App\Modelos;

use Illuminate\Support\Facades\DB;

class Daocategoria
{

    private $query = 'select * from categoria';
    private $listacategorias;

    public function __construct()
    {
    }

    public function getCategorias()
    {

        $this->listacategorias = DB::select($this->query);
        return $this->listacategorias;
    }

    public function setCategoria($c)
    {
        DB::insert('insert into categoria (nombre_categoria,descripcion, img ,visible) 
            values ( :nombre_categoria , :descripcion, :img ,:visible)', $c);
    }

    public function getCategoria($id)
    {
        $categoria = DB::select('select * from categoria where id_categoria = :id', ['id' => $id]);
        return $categoria;
    }

    public function updateCategoria($c)
    {
        DB::table('categoria')
            ->where('id_categoria', $c['id_categoria'])
            ->update($c); 
    }

    public function deleteCategoria($id)
    {

        DB::statement('delete from producto where id_subcategoria in 
        (select p.id_subcategoria from (categoria c join sub_categoria s on c.id_categoria=s.id_categoria)
        left join producto p on s.id_subcategoria=p.id_subcategoria where c.id_categoria= :id)', ['id' => $id]);
        DB::table('sub_categoria')->where('id_categoria', '=', $id)->delete();
        DB::table('categoria')->where('id_categoria', '=', $id)->delete();
    }
}
