<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Catalogo;
use App\Modelos\Producto;
use App\Modelos\Subcategoria;

class ControlCatalogo extends Controller
{
  
    public function __construct()
    {
        //$this->middleware('guest'); 
        $this->middleware('auth');
    }

    public function index()
    {
        $index= new Catalogo();
        $categorias= $index->AddCategorias();
        return view('admin',compact('categorias'));
    }
    public function index2()
    {
        $index= new Catalogo();
        $categorias= $index->AddCategorias();
        return view('admin.VerCatalogo',compact('categorias'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if("categoria" === $request->get('id'))
        {
            $controlC=new ControlCategoria();
            $controlC->store($request);
            return redirect()->route('Vercatalogo')->with('estado', 'La categoría se ha creado con exito');

        }elseif ("subcategoria"===$request->get('id')){

            $controlS=new ControlSubcategoria();
            $controlS->store($request);
            return redirect()->route('Vercatalogo')->with('estado', 'La subcategoría se ha creado con exito');

        }elseif ("producto"===$request->get('id'))
        {
            $controlP=new ControlProducto();
            $controlP->store($request);
            return redirect()->route('Vercatalogo')->with('estado', 'El producto se ha creado con exito');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
