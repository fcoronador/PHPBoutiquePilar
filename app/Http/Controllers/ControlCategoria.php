<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Categoria;

class ControlCategoria extends Controller
{

    private $Categoria;
    public function __construct()
    {
        $this->Categoria = new Categoria();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre_categoria' => 'required',
            'descripcion' => 'required',
            'img'=>'required'
        ],
        [
            'nombre_categoria.required' => 'Se necesita nombre de la categoría',
            'descripcion.required' => 'Se necesita la descripción de la categoría',
            'img.required' => 'Se necesita la url imagen de la categoría'
             //mensajes de error personalizados
         ]);
        $c['nombre_categoria'] = $request->get('nombre_categoria');
        $c['descripcion'] = $request->get('descripcion');
        $c['img'] = $request->get('img');
        $c['visible'] = $request->get('visible');
        $this->Categoria->GuardarCategoria($c);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = $this->Categoria->SeleccionarCategoria($id);
        return view('categoria.edit', compact('cat', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        request()->validate([
            'nombre_categoria' => 'required',
            'descripcion' => 'required',
            'img'=>'required'
        ],
        [
            'nombre_categoria.required' => 'Se necesita nombre de la categoria',
            'descripcion.required' => 'Se necesita la descripción de la categoria',
            'img.required' => 'Se necesita la url imagen de la categoría'
             //mensajes de error personalizados
         ]);

        $c['id_categoria'] = $id;
        $c['nombre_categoria'] = $request->get('nombre_categoria');
        $c['descripcion'] = $request->get('descripcion');
        $c['img'] = $request->get('img');
        $c['visible'] = $request->get('visible');
        $this->Categoria->ActualizarCategoria($c);
        return redirect()->route('Vercatalogo')->with('estado', 'La categoría se ha actualizado con exito');
    }

    public function delete(Request $request, $id)
    {

        $c['id_categoria'] = $id;
        $c['visible'] = $request->get('visible');
        $this->Categoria->ActualizarCategoria($c);
        return redirect()->route('Vercatalogo')->with('estado', 'La categoría se ha eliminado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->Categoria->BorrarCategoria($id);
        return redirect()->route('Vercatalogo')->with('estado', 'La categoría se ha eliminado con exito');
    }
}
