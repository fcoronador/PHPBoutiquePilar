<?php

namespace App\Http\Controllers;

use App\Mail\ControlCorreos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/* Pendidente configuracion para paso produccion */
/* Desarrollo tiene mailtrap */
class ControlContacto extends Controller
{
    public function store(Request $request){

        $mensaje=request()->validate([// request()hace opcional cargar Request $request
            'name' => 'required',
            'email'=> 'required |email ',
            'asunto'=> 'required',
            'content'=> 'required | min:3' // min:3 declara que tenga minimo tres caracteres
            
         ],[
            'name.required' => 'El campo nombre es necesario' //mensajes de error personalizados
         ]);
         
         Mail::to('b.femeninapilar@gmail.com')->queue(new ControlCorreos($mensaje)); //correo del dueno de la pagina
         Mail::to($mensaje['email'])->queue(new ControlCorreos($mensaje)); // Envia el mismo correo a los dos cliente como dueno

        return back()->with('estado','Recibimos tu mensaje, te responderemos en 24 horas');
    }
}
