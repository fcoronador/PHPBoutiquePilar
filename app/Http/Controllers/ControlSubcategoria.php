<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Subcategoria;
use App\Modelos\Producto;

class ControlSubcategoria extends Controller
{

    private $Subcategoria;
    private $Producto; 
    public function __construct()
    {
        $this->Subcategoria = new Subcategoria();
        $this->Producto = new Producto();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $listasubcategorias = $this->Subcategoria->listaSubcategoria();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
            [
                'nombre_subcategoria' => 'required',
                'descripcion' => 'required',
                'img' => 'required'
            ],
            [
                'nombre_subcategoria.required' => 'Se necesita nombre de la subcategoría.',
                'descripcion.required' => 'Se necesita la descripción de la subcategoría.',
                'img.required' => 'Se necesita la url de la imagen de la subcategoría.',
                //mensajes de error personalizados
            ]
        );
        $s['id_categoria'] = $request->get('id_categoria');
        $s['nombre_subcategoria'] = $request->get('nombre_subcategoria');
        $s['descripcion'] = $request->get('descripcion');
        $s['img'] = $request->get('img');
        $s['visible'] = $request->get('visible');
        $this->Subcategoria->GuardarSubcategoria($s);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subcat = $this->Subcategoria->SeleccionarSubcategoria($id);
        $album =  $this->Producto->SeleccionarAlbum();
        return view('producto', compact('subcat', 'id', 'album')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcate = $this->Subcategoria->SelectSubcategoria($id); 

        return view('subcategoria.edit', compact('subcate', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(
            [
                'nombre_subcategoria' => 'required',
                'descripcion' => 'required',
                'img' => 'required'
            ],
            [
                'nombre_subcategoria.required' => 'Se necesita nombre de la subcategoria',
                'descripcion.required' => 'Se necesita la descripción de la subcategoria',
                'img.required' => 'Se necesita la url de la imagen de la subcategoría.',
                //mensajes de error personalizados
            ]
        ); 

        $s['id_categoria'] = $request->get('id_categoria');
        $s['id_subcategoria'] = $id;
        $s['nombre_subcategoria'] = $request->get('nombre_subcategoria');
        $s['descripcion'] = $request->get('descripcion');
        $s['img'] = $request->get('img');
        $s['visible'] = $request->get('visible');
        $this->Subcategoria->ActualizarSubcategoria($s);
        return redirect()->route('Vercatalogo')->with('estado', 'La subcategoria se ha actualizado con exito');
    }

    public function delete(Request $request, $id)
    {

        $s['id_subcategoria'] = $id;
        $s['visible'] = $request->get('visible');
        $this->Subcategoria->ActualizarSubcategoria($s);
        return redirect()->route('Vercatalogo')->with('estado', 'La subcategoría se ha eliminado con exito');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->Subcategoria->EliminarSubcategoria($id);
        return redirect()->route('Vercatalogo')->with('estado', 'La subcategoria se ha eliminado con exito');
    }
}
