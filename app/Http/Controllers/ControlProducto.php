<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Producto;

class ControlProducto extends Controller
{

    private $Producto;
    public function __construct()
    {
        $this->Producto = new Producto();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre_producto' => 'required',
            'descripcion' => 'required',
            'valor_unitario' => 'required',
            'precio_venta' => 'required',
            'referencia' => 'required',
            'material' => 'required',
            'medida' => 'required',
            'img' => 'required',
        ],
        [
            'nombre_producto.required' => 'Se necesita nombre del producto',
            'descripcion.required' => 'Se necesita la descripción del producto',
            'valor_unitario.required' => 'Se necesita el valor unitario del producto',
            'precio_venta.required' => 'Se necesita el precio de venta del producto',
            'referencia.required' => 'Se necesita la referencia del producto',
            'material.required' => 'Se necesita el material del producto',
            'medida.required' => 'Se necesita la medida del producto',
            'img.required' => 'Se necesita la imagen del producto',
             //mensajes de error personalizados
         ]);
        
        $p['id_subcategoria'] = $request->get('id_subcategoria');
        $p['nombre_producto'] = $request->get('nombre_producto');
        $p['descripcion'] = $request->get('descripcion');
        $p['valor_unitario'] = $request->get('valor_unitario');
        $p['precio_venta'] = $request->get('precio_venta');
        $p['referencia'] = $request->get('referencia');
        $p['material'] = $request->get('material');
        $p['medida'] = $request->get('medida');
        $p['visible'] = $request->get('visible');
        $p['img'] = $request->get('img');
        $p['comprar'] = $request->get('comprar');
        $this->Producto->Guardarproducto($p);
        /* $imagen = $request->file('img');
        $imagen->move('img', $imagen->getClientOriginalName());
        $p['img'] = "/img/" . $imagen->getClientOriginalName(); */

        $imagenes['img2'] = $request->get('img2');
        $imagenes['img3'] = $request->get('img3');
        $imagenes['img4'] = $request->get('img4');
        $imagenes['img5'] = $request->get('img5');
        $imagenes['img6'] = $request->get('img6');
        $imagenes['img7'] = $request->get('img7');
        $imagenes['img8'] = $request->get('img8');
        $this->Producto->Album($imagenes);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $prenda = $this->Producto->SeleccionarProducto($id);
        return view('producto.show', compact('prenda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prenda = $this->Producto->SeleccionarProducto($id);
        $album = $this->Producto->AlbumProducto($id);
        
        
        return view('producto.edit', compact('prenda', 'album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'nombre_producto' => 'required',
            'descripcion' => 'required',
            'valor_unitario' => 'required',
            'precio_venta' => 'required',
            'referencia' => 'required',
            'material' => 'required',
            'medida' => 'required',
            'img' => 'required',
        ],
        [
            'nombre_producto.required' => 'Se necesita nombre del producto',
            'descripcion.required' => 'Se necesita la descripción del producto',
            'valor_unitario.required' => 'Se necesita el valor unitario del producto',
            'precio_venta.required' => 'Se necesita el precio de venta del producto',
            'referencia.required' => 'Se necesita la referencia del producto',
            'material.required' => 'Se necesita el material del producto',
            'medida.required' => 'Se necesita la medida del producto',
            'img.required' => 'Se necesita la imagen del producto',
             //mensajes de error personalizados
         ]);
        
        $p['id_subcategoria'] = $request->get('id_subcategoria');
        $p['id_producto'] = $request->get('id_producto');
        $p['nombre_producto'] = $request->get('nombre_producto');
        $p['descripcion'] = $request->get('descripcion');
        $p['valor_unitario'] = $request->get('valor_unitario');
        $p['precio_venta'] = $request->get('precio_venta');
        $p['referencia'] = $request->get('referencia');
        $p['material'] = $request->get('material');
        $p['medida'] = $request->get('medida');
        $p['visible'] = $request->get('visible');
        $p['img'] = $request->get('img');
        $p['comprar'] = $request->get('comprar');
        /* $imagen = $request->file('img');
        $imagen->move('img', $imagen->getClientOriginalName());
        $p['img'] = "/img/" . $imagen->getClientOriginalName(); */
        
        $imagenes['id_producto'] = $request->get('id_producto');
        $imagenes['img2'] = $request->get('img2');
        $imagenes['img3'] = $request->get('img3');
        $imagenes['img4'] = $request->get('img4');
        $imagenes['img5'] = $request->get('img5');
        $imagenes['img6'] = $request->get('img6');
        $imagenes['img7'] = $request->get('img7');
        $imagenes['img8'] = $request->get('img8');
        $this->Producto->UAlbum($imagenes);

        $this->Producto->Modificarproducto($p);
        return redirect()->route('Vercatalogo')->with('estado', 'El producto se ha actualizado con exito');
    }

    public function delete(Request $request, $id)
    {

        $p['id_producto'] = $id;
        $p['visible'] = $request->get('visible');
        $this->Producto->Modificarproducto($p);
        return redirect()->route('Vercatalogo')->with('estado', 'El producto se ha eliminado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->Producto->borrarProducto($id);
        return redirect()->route('Vercatalogo')->with('estado', 'El producto se ha eliminado con exito');
    }
}
