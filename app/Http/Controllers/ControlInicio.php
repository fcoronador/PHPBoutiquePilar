<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Producto;

class ControlInicio extends Controller
{
    
    public function index()
    {
        return view('inicio2');
    }


    public function nuevo()
    {
        return view('inicio');
    }
    public function subcategorias($id)
    {
        
        return view('subcategoria', compact('id'));
    }
 

}
