<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modelos\Categoria;
use App\Modelos\Subcategoria;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       
        $categoria=new Categoria();
        $subcategoria=new Subcategoria();
        $clist=$categoria->CalcularCategorias(); 
        $slist=$subcategoria->CalcularSubcategorias();
        view()->share(compact(['clist','slist']));
    }
}
