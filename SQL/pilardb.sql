/* ----------------------------USUARIOS-------------------------- */
create table persona(
id_persona serial primary key,
nombre_razonsocial character varying(30),
fechaNacimiento date,
edad int,
email character varying(30),
tipoPersona character(1),
direccion character varying(30)
);
create table empleado(
id_empleado serial,
ide_persona integer references persona(id_persona),
comision double precision,
foreign key (ide_persona) references persona(id_persona),
primary key(id_empleado,ide_persona)
);

create table cliente(
id_cliente serial,
ide_persona integer references persona(id_persona),
frecuente boolean, /*frecuente  si o no */
foreign key (ide_persona) references persona(id_persona),
primary key(id_cliente,ide_persona)
);

create table proveedor(
id_proveedor serial,
ide_persona integer references persona(id_persona),
tipo character(1), /*debito D o credito C*/
foreign key (ide_persona) references persona(id_persona),
primary key(id_proveedor,ide_persona)
);

create table ciudad(
id_ciudad serial primary key,
nombreciudad character varying(20)
);

create table barrio(
	id_barrio serial primary key,
	nombrebarrio character varying(10)
);

create table puntoVenta(
id_puntoventa serial primary key,
telefono int,
nombrePunt character varying(30)
);


create table personaXbarrio(
ide_persona integer,
ide_barrio integer,
foreign key (ide_persona) references persona(id_persona),
foreign key (ide_barrio) references barrio(id_barrio)
);
alter table personaxbarrio add primary key(ide_persona,ide_barrio);

create table personabarrioXciudad( --tabla que que relaciona los bariios, las ciudades y las personas 
ide_persona integer,
ide_barrio integer,
ide_ciudad integer,
foreign key (ide_persona,ide_barrio) references personaXbarrio(ide_persona,ide_barrio),
foreign key (ide_ciudad) references ciudad(id_ciudad)
);

create table puntobarriociudad(
ide_puntoventa integer references puntoVenta(id_puntoventa),
ide_barrio integer references barrio(id_barrio),
ide_ciudad integer references ciudad(id_ciudad),
foreign key (ide_puntoventa) references puntoVenta(id_puntoventa),
foreign key (ide_barrio) references barrio(id_barrio),
foreign key (ide_ciudad) references ciudad(id_ciudad)
);


/* ------------------------PRODUCTO-CATALOGO--------------------- */
create table producto(
id_producto serial primary key,
ref character varying(20),
nombreProd character varying(80),
material character varying(20),
precioventa double precision check (precioventa > 0),
costoUnitario double precision check (costoUnitario > 0),
medida character varying(20) not null,
imagen character varying(100)
);

create table categoria(
id_Categoria  serial  primary key,
nombreCate character varying(20)
);

create table subcategoria(
id_subcategoria serial primary key,
nombreSubCat character varying(20),
descripcion character varying(30)
);

create table categoriaXproducto(  
ide_producto integer references producto (id_producto) ,
ide_categoria  integer references categoria(id_categoria),
foreign key (ide_categoria) references categoria (id_categoria),
foreign key (ide_producto) references producto (id_producto),
primary key(ide_producto,ide_categoria)
);

create table produc_categoXsubcatego( --La consulta sole debera traer los productos que tengan su subcategoria
ide_producto integer,
ide_categoria  integer,
ide_subcategoria integer,
foreign key (ide_producto, ide_categoria) references categoriaXproducto(ide_producto,ide_categoria),
foreign key (ide_subcategoria) references subcategoria (id_subcategoria),
primary key(ide_producto,ide_categoria,ide_subcategoria)
);
/* ------------------------TRANSACCION-INVENTARIO---------------- */

create table transacc(
id_transacc serial primary key,
tipo_transacc  boolean, /* compra o venta*/
fecha_transa date,
total double precision check (total > 0),
subtotal double precision check  (subtotal > 0),
estado boolean,  /*pagado o en deuda */
cantidad int
);

create table productotransac( --TABLA DE RELACION QUE PERMITE SABER QUE PRODUCTOS HAS SIDO TRANSACCIONADOS
ide_producto integer,
ide_categoria integer,
ide_subcategoria integer,
ide_transacc integer references transacc(id_transacc),
foreign key(ide_producto,ide_categoria,ide_subcategoria) references produc_categoXsubcatego(ide_producto,ide_categoria,ide_subcategoria),--La relacion significa que solo procutos con subcategoria se pueden transaccionar
foreign key (ide_transacc) references transacc(id_transacc),
primary key(ide_producto,ide_categoria,ide_subcategoria,ide_transacc)
);

create table puntoXtransacc( --TABLA RELACION PERMITE IDENTIFICAR Los productos vendidos en los puntos de venta
ide_puntoventa integer references puntoVenta(id_puntoventa),
ide_producto integer,
ide_categoria integer,
ide_subcategoria integer,
ide_transacc integer,
foreign key(ide_producto,ide_categoria,ide_subcategoria,ide_transacc) references productotransac(ide_producto,ide_categoria,ide_subcategoria,ide_transacc),
foreign key (ide_puntoventa) references puntoVenta(id_puntoventa),
primary key (ide_producto,ide_categoria,ide_subcategoria,ide_transacc,ide_puntoventa)
);


