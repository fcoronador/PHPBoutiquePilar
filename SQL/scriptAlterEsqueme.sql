/**
 * Author:Alvaro Felipe Coronado
 * Date:11/04/2020
 * Description: Asiging the foreignkey to tables in the shema
 */

ALTER TABLE barrio
ADD CONSTRAINT FK_BARRIO_ID_CIUDAD 
FOREIGN KEY  (id_ciudad)  REFERENCES ciudad(id_ciudad);

ALTER TABLE persona
ADD CONSTRAINT FK_PERSONA_ID_BARRIO
FOREIGN KEY (id_barrio) REFERENCES barrio(id_barrio);

ALTER TABLE persona
ADD CONSTRAINT FK_TIPO_DOCU_ID_TIPO_DOCUMENTO
FOREIGN KEY (id_tipo_documento)  REFERENCES tipo_documento(id_tipo_documento);

ALTER TABLE persona
add CONSTRAINT UQ_PERSONA_EMAIL
unique (email);

ALTER TABLE sub_categoria
ADD CONSTRAINT FK_SUBCATE_ID_CATEGORIA
FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria);

ALTER TABLE producto
ADD CONSTRAINT FK_PRODUCTO_ID_SUBCATE
FOREIGN KEY (id_subcategoria) REFERENCES sub_categoria(id_subcategoria);

ALTER TABLE ubicado
ADD CONSTRAINT FK_UBICADO_ID_BARRIO
FOREIGN KEY (id_barrio) REFERENCES barrio(id_barrio);


ALTER TABLE ubicado
ADD CONSTRAINT FK_UBICADO_ID_PUNTO_VENTA
FOREIGN KEY (id_punto_venta) REFERENCES punto_venta (id_punto_venta);

ALTER TABLE producto_ubicado
ADD CONSTRAINT FK_PRODUCTOUBI_ID_PRODUCTO
FOREIGN KEY (id_producto) REFERENCES producto(id_producto);

ALTER TABLE producto_ubicado
ADD CONSTRAINT FK_PRODUCTOUBI_ID_PUNTO_VENTA
FOREIGN KEY (id_punto_venta) REFERENCES punto_venta(id_punto_venta);


ALTER TABLE imagenes
ADD CONSTRAINT FK_PRODUCTO_ID_IMAGEN
FOREIGN KEY (id_producto) REFERENCES producto(id_producto);

