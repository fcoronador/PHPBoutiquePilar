
ALTER TABLE "categoriaxproducto"
DROP CONSTRAINT "categoriaxproducto_ide_producto_fkey";
ALTER TABLE "categoriaxproducto"
DROP CONSTRAINT "categoriaxproducto_ide_producto_fkey1";
ALTER TABLE "categoriaxproducto"
DROP CONSTRAINT "categoriaxproducto_ide_categoria_fkey";
ALTER TABLE "categoriaxproducto"
DROP CONSTRAINT "categoriaxproducto_ide_categoria_fkey1";

ALTER TABLE "cliente"
DROP CONSTRAINT "cliente_ide_persona_fkey";
ALTER TABLE "cliente"
DROP CONSTRAINT "cliente_ide_persona_fkey1";

ALTER TABLE "empleado"
DROP CONSTRAINT "empleado_ide_persona_fkey";
ALTER TABLE "empleado"
DROP CONSTRAINT "empleado_ide_persona_fkey1";
ALTER TABLE "personabarrioxciudad"
DROP CONSTRAINT "personabarrioxciudad_ide_persona_fkey";
ALTER TABLE "personabarrioxciudad"
DROP CONSTRAINT "personabarrioxciudad_ide_ciudad_fkey";

ALTER TABLE "personaxbarrio"
DROP CONSTRAINT "personaxbarrio_ide_persona_fkey";
ALTER TABLE "personaxbarrio"
DROP CONSTRAINT "personaxbarrio_ide_barrio_fkey";

ALTER TABLE "produc_categoxsubcatego"
DROP CONSTRAINT "produc_categoxsubcatego_ide_producto_fkey";
ALTER TABLE "produc_categoxsubcatego"
DROP CONSTRAINT "produc_categoxsubcatego_ide_subcategoria_fkey";

ALTER TABLE "productotransac"
DROP CONSTRAINT "productotransac_ide_producto_fkey";
ALTER TABLE "productotransac"
DROP CONSTRAINT "productotransac_ide_transacc_fkey";
ALTER TABLE "productotransac"
DROP CONSTRAINT "productotransac_ide_transacc_fkey1";

ALTER TABLE "proveedor"
DROP CONSTRAINT "proveedor_ide_persona_fkey";
ALTER TABLE "proveedor"
DROP CONSTRAINT "proveedor_ide_persona_fkey1";

ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_puntoventa_fkey";
ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_puntoventa_fkey1";
ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_barrio_fkey";
ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_barrio_fkey1";
ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_ciudad_fkey";
ALTER TABLE "puntobarriociudad"
DROP CONSTRAINT "puntobarriociudad_ide_ciudad_fkey1";

ALTER TABLE "puntoxtransacc"
DROP CONSTRAINT "puntoxtransacc_ide_puntoventa_fkey";
ALTER TABLE "puntoxtransacc"
DROP CONSTRAINT "puntoxtransacc_ide_puntoventa_fkey1";
ALTER TABLE "puntoxtransacc"
DROP CONSTRAINT "puntoxtransacc_ide_producto_fkey";

DROP TABLE IF EXISTS "barrio";
DROP TABLE IF EXISTS "categoria";
DROP TABLE IF EXISTS "categoriaxproducto";
DROP TABLE IF EXISTS "ciudad";
DROP TABLE IF EXISTS "cliente";
DROP TABLE IF EXISTS "empleado";
DROP TABLE IF EXISTS "persona";
DROP TABLE IF EXISTS "personabarrioxciudad";
DROP TABLE IF EXISTS "personaxbarrio";
DROP TABLE IF EXISTS "produc_categoxsubcatego";
DROP TABLE IF EXISTS "producto";
DROP TABLE IF EXISTS "productotransac";
DROP TABLE IF EXISTS "proveedor";
DROP TABLE IF EXISTS "puntobarriociudad";
DROP TABLE IF EXISTS "puntoventa";
DROP TABLE IF EXISTS "puntoxtransacc";
DROP TABLE IF EXISTS "subcategoria";
DROP TABLE IF EXISTS "transacc";



DROP SEQUENCE IF EXISTS categoria_id_categoria_seq;
DROP SEQUENCE IF EXISTS barrio_id_barrio_seq;
DROP SEQUENCE IF EXISTS ciudad_id_ciudad_seq;
DROP SEQUENCE IF EXISTS cliente_id_cliente_seq;
DROP SEQUENCE IF EXISTS empleado_id_empleado_seq;
DROP SEQUENCE IF EXISTS persona_id_persona_seq;
DROP SEQUENCE IF EXISTS producto_id_producto_seq;
DROP SEQUENCE IF EXISTS proveedor_id_proveedor_seq;
DROP SEQUENCE IF EXISTS subcategoria_id_subcategoria_seq;
DROP SEQUENCE IF EXISTS puntoventa_id_puntoventa_seq;
DROP SEQUENCE IF EXISTS transacc_id_transacc_seq;




