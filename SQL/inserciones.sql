insert into categoria (nombrecate) values ('Prendas');
insert into categoria (nombrecate) values ('Accesorios');
insert into categoria (nombrecate) values ('Zapatos');

insert into subcategoria(nombresubcat,descripcion) values('Ninguna','No hay subcategorias');
insert into subcategoria(nombresubcat,descripcion) values('Blusas','Blusas en todas las tallas');
insert into subcategoria(nombresubcat,descripcion) values('Batas','Batas de todos los colores');
insert into subcategoria(nombresubcat,descripcion) values('Pantalones','Pantalones super lindos');
insert into subcategoria(nombresubcat,descripcion) values('Chaquetas','Chaquetas geniales');
insert into subcategoria(nombresubcat,descripcion) values('Gabardinas','Gabardinas hasta la cintura');

insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0040','Pantalon','supervertigo',10800.10,5302.1,'6-16');
insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0050','Bata corte X','supervertigo',10800.10,5302.1,'6-16');
insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0060','Pantalon corte y','algodon',10800.10,5302.1,'6-16');
insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0080','Flor','mixtos',108.10,52.1,'0');
insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0070','chaqueta corte princesa','cachimir',10800.10,5302.1,'6-16');
insert into producto (ref,nombreprod,material,precioventa,costounitario,medida)values ('RF0010','Zapato Gamusa','Gamusa',10800.10,5302.1,'8-43');

insert into CategoriaXProducto (ide_producto,ide_categoria) values(1,1);
insert into CategoriaXProducto (ide_producto,ide_categoria) values(2,1);
insert into CategoriaXProducto (ide_producto,ide_categoria) values(4,2);
insert into CategoriaXProducto (ide_producto,ide_categoria) values(6,3);

insert into produc_CategoXsubCatego (ide_producto,ide_categoria,ide_subcategoria) values(1,1,4);
insert into produc_CategoXsubCatego (ide_producto,ide_categoria,ide_subcategoria) values(2,1,3);

insert into produc_CategoXsubCatego (ide_producto,ide_categoria,ide_subcategoria) values(4,2,1);
insert into produc_CategoXsubCatego (ide_producto,ide_categoria,ide_subcategoria) values(6,3,1);