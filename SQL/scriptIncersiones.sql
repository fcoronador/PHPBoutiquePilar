insert into categoria (nombre_categoria,descripcion, visible) values ('Prendas','sobre la categoria', true);
insert into categoria (nombre_categoria,descripcion, visible) values ('Accesorios','sobre la categoria', true);
insert into categoria (nombre_categoria,descripcion, visible) values ('Zapatos','sobre la categoria', true);

insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(2,'Flores','Accesorios finos para la ropa', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(1,'Blusas','Blusas en todas las tallas', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(1,'Batas','Batas de todos los colores', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(1,'Pantalones','Pantalones super lindos', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(1,'Chaquetas','Chaquetas geniales', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(1,'Gabardinas','Gabardinas hasta la cintura', true);
insert into sub_categoria(id_categoria,nombre_subcategoria,descripcion, visible) values(3,'Elegantes','Zapatos finos para dama', true);

insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (4,'RF0040','Pantalon','supervertigo',10800.10,5302.1,'6-16','/img/01.svg', true);
insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (3,'RF0050','Bata corte X','supervertigo',10800.10,5302.1,'6-16','/img/01.svg', true);
insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (4,'RF0060','Pantalon corte y','algodon',10800.10,5302.1,'6-16','/img/01.svg', true);
insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (1,'RF0080','Flor','mixtos',108.10,52.1,'0','/img/01.svg', true);
insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (5,'RF0070','chaqueta corte princesa','cachimir',10800.10,5302.1,'6-16','/img/01.svg', true);
insert into producto (id_subcategoria, referencia,nombre_producto,material,precio_venta,valor_unitario,medida,img,visible)values (7,'RF0010','Zapato Gamusa','Gamusa',10800.10,5302.1,'8-43','/img/01.svg', true);