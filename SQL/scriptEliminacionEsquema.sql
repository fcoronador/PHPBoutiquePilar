/**
 * Author:Alvaro Felipe Coronado
 * Date:11/04/2020
 * Description: drop shema database
 */

DROP TABLE producto_ubicado;

DROP TABLE ubicado;

DROP TABLE punto_venta;

DROP TABLE producto;

DROP TABLE sub_categoria;

DROP TABLE categoria;

DROP TABLE persona;

DROP TABLE barrio;

DROP TABLE ciudad;

DROP TABLE tipo_documento;
DROP TABLE imagenes;





