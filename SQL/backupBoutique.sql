-- Adminer 4.7.7 PostgreSQL dump





DROP TABLE IF EXISTS "categoria";
DROP SEQUENCE IF EXISTS categoria_id_categoria_seq;
CREATE SEQUENCE categoria_id_categoria_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 4 CACHE 1;

CREATE TABLE "public"."categoria" (
    "id_categoria" integer DEFAULT nextval('categoria_id_categoria_seq') NOT NULL,
    "nombre_categoria" character varying(100),
    "descripcion" character varying(100),
    "visible" boolean,
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    "creado_por" character varying(50),
    "editado_por" character varying(50),
    "img" character varying(100),
    CONSTRAINT "categoria_pkey" PRIMARY KEY ("id_categoria")
) WITH (oids = false);

INSERT INTO "categoria" ("id_categoria", "nombre_categoria", "descripcion", "visible", "fecha_creacion", "fecha_actualizacion", "creado_por", "editado_por", "img") VALUES
(1,	'Prendas',	'Prendas elegantes',	'1',	NULL,	NULL,	NULL,	NULL,	'https://i.imgur.com/zNDU2jY.jpg'),
(2,	'Accesorios',	'Los accesorios más bonitos',	'1',	NULL,	NULL,	NULL,	NULL,	'https://i.imgur.com/mnyMd6Y.jpg'),
(3,	'Uniformes',	'Uniformes para dotación',	'1',	NULL,	NULL,	NULL,	NULL,	'https://i.imgur.com/heyU1zo.jpg'),
(4,	'Conjuntos',	'Los conjuntos más bonitos',	'1',	NULL,	NULL,	NULL,	NULL,	'https://i.imgur.com/OqvR8l5.jpg');

DROP TABLE IF EXISTS "ciudad";
DROP SEQUENCE IF EXISTS ciudad_id_ciudad_seq;
CREATE SEQUENCE ciudad_id_ciudad_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."ciudad" (
    "id_ciudad" integer DEFAULT nextval('ciudad_id_ciudad_seq') NOT NULL,
    "nombre_ciudad" character varying(50),
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    "creado_por" character varying(50),
    CONSTRAINT "ciudad_pkey" PRIMARY KEY ("id_ciudad")
) WITH (oids = false);





DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 2 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0)
) WITH (oids = false);

CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");


DROP TABLE IF EXISTS "barrio";
DROP SEQUENCE IF EXISTS barrio_id_barrio_seq;
CREATE SEQUENCE barrio_id_barrio_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."barrio" (
    "id_barrio" integer DEFAULT nextval('barrio_id_barrio_seq') NOT NULL,
    "id_ciudad" integer,
    "nombre_barrio" character varying(50),
    "puntos_referencia" character varying(150),
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    "creado_por" character varying(50),
    CONSTRAINT "barrio_pkey" PRIMARY KEY ("id_barrio"),
    CONSTRAINT "fk_barrio_id_ciudad" FOREIGN KEY (id_ciudad) REFERENCES ciudad(id_ciudad) NOT DEFERRABLE
) WITH (oids = false);

DROP TABLE IF EXISTS "tipo_documento";
DROP SEQUENCE IF EXISTS tipo_documento_id_tipo_documento_seq;
CREATE SEQUENCE tipo_documento_id_tipo_documento_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."tipo_documento" (
    "id_tipo_documento" integer DEFAULT nextval('tipo_documento_id_tipo_documento_seq') NOT NULL,
    "nombre_tipo_documento" character varying(100),
    CONSTRAINT "tipo_documento_pkey" PRIMARY KEY ("id_tipo_documento")
) WITH (oids = false);


DROP TABLE IF EXISTS "persona";
DROP SEQUENCE IF EXISTS persona_id_persona_seq;
CREATE SEQUENCE persona_id_persona_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."persona" (
    "id_persona" integer DEFAULT nextval('persona_id_persona_seq') NOT NULL,
    "id_barrio" integer,
    "id_tipo_documento" integer,
    "email" character varying(150),
    "numero_identificacion" character varying(20),
    "nombre" character varying(100),
    "estado" character varying(1),
    "rol" character varying(3),
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    CONSTRAINT "persona_pkey" PRIMARY KEY ("id_persona"),
    CONSTRAINT "uq_persona_email" UNIQUE ("email"),
    CONSTRAINT "fk_persona_id_barrio" FOREIGN KEY (id_barrio) REFERENCES barrio(id_barrio) NOT DEFERRABLE,
    CONSTRAINT "fk_tipo_docu_id_tipo_documento" FOREIGN KEY (id_tipo_documento) REFERENCES tipo_documento(id_tipo_documento) NOT DEFERRABLE
) WITH (oids = false);

DROP TABLE IF EXISTS "sub_categoria";
DROP SEQUENCE IF EXISTS sub_categoria_id_subcategoria_seq;
CREATE SEQUENCE sub_categoria_id_subcategoria_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 12 CACHE 1;

CREATE TABLE "public"."sub_categoria" (
    "id_subcategoria" integer DEFAULT nextval('sub_categoria_id_subcategoria_seq') NOT NULL,
    "id_categoria" integer,
    "nombre_subcategoria" character varying(50),
    "descripcion" character varying(100),
    "visible" boolean,
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    "creado_por" character varying(50),
    "editado_por" character varying(50),
    "img" character varying(100),
    CONSTRAINT "sub_categoria_pkey" PRIMARY KEY ("id_subcategoria"),
    CONSTRAINT "fk_subcate_id_categoria" FOREIGN KEY (id_categoria) REFERENCES categoria(id_categoria) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "sub_categoria" ("id_subcategoria", "id_categoria", "nombre_subcategoria", "descripcion", "visible", "fecha_creacion", "fecha_actualizacion", "creado_por", "editado_por", "img") VALUES
(2,	2,	'Flores',	'Las flores mas llamativas',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(3,	3,	'Empresarial',	'Uniformes para dotación empresarial',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	1,	'Camisetas',	'Camisetas de todos los tipos',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	1,	'Pantalones',	'Pantalones de calidad',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	1,	'Gabanes',	'Gabanes de calidad',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	1,	'Enterizos',	'Los enterizos más elegantes',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(8,	1,	'Chaquetas',	'Chaquetas de calidad',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(10,	4,	'Formales',	'Conjuntos hermosos',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(9,	1,	'Blusas',	'Las blusas más bonitas',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(11,	1,	'Batas',	'Vestidos sin mangas y con falda a la rodilla',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(12,	1,	'Faldas',	'Faldas elegantes.',	'1',	NULL,	NULL,	NULL,	NULL,	NULL),
(1,	1,	'Tops',	'Tops de encajes licrados',	'1',	NULL,	NULL,	NULL,	NULL,	'https://i.imgur.com/hnV1aIF.jpg');




DROP TABLE IF EXISTS "producto";
DROP SEQUENCE IF EXISTS producto_id_producto_seq;
CREATE SEQUENCE producto_id_producto_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 22 CACHE 1;

CREATE TABLE "public"."producto" (
    "id_producto" integer DEFAULT nextval('producto_id_producto_seq') NOT NULL,
    "id_subcategoria" integer,
    "nombre_producto" character varying(100),
    "descripcion" character varying(2000),
    "valor_unitario" numeric(13,3),
    "precio_venta" numeric(13,3),
    "referencia" character varying(50),
    "material" character varying(50),
    "medida" character varying(50),
    "img" character varying(100),
    "comprar" character varying(300),
    "visible" boolean,
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    "creado_por" character varying(50),
    "editado_por" character varying(50),
    CONSTRAINT "producto_pkey" PRIMARY KEY ("id_producto"),
    CONSTRAINT "fk_producto_id_subcate" FOREIGN KEY (id_subcategoria) REFERENCES sub_categoria(id_subcategoria) NOT DEFERRABLE
) WITH (oids = false);

INSERT INTO "producto" ("id_producto", "id_subcategoria", "nombre_producto", "descripcion", "valor_unitario", "precio_venta", "referencia", "material", "medida", "img", "comprar", "visible", "fecha_creacion", "fecha_actualizacion", "creado_por", "editado_por") VALUES
(2,	2,	'Flor',	'Broche en forma de flor',	6000.000,	12000.000,	'F-001',	'Polyester',	'4 cm',	'https://i.imgur.com/mnyMd6Y.jpg',	'https://articulo.mercadolibre.com.co/MCO-569030929-flores-decorativas-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(4,	4,	'Camiseta Polo',	'Camiseta estilo polo',	16000.000,	30000.000,	'Ctp-001',	'Polyester y algodón',	'S-XL',	'https://i.imgur.com/rMtqTRn.jpg',	'https://articulo.mercadolibre.com.co/MCO-569046758-camiseta-polo-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(3,	3,	'Uniforme básico',	'Un uniforme conformado por camiseta polo y pantalón de paño.',	75000.000,	80000.000,	'Ue-001',	'Polyester y algodon',	'S-XL',	'https://i.imgur.com/heyU1zo.jpg',	'https://articulo.mercadolibre.com.co/MCO-569046925-uniforme-empresarial-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(11,	9,	'Blusas a rayas',	'Las blusas más bonitas',	37500.000,	50000.000,	'Br-001',	'Polyester y algodón',	'8-20',	'https://i.imgur.com/fkhP09R.jpg',	'https://articulo.mercadolibre.com.co/MCO-569167432-2-blusa-a-rayas-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(1,	1,	'Top',	'Tops de encajes licrados',	17000.000,	35000.000,	'T-001',	'Lycra',	'6-22',	'https://i.imgur.com/hnV1aIF.jpg',	'https://articulo.mercadolibre.com.co/MCO-569139718-tops-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(6,	6,	'Gabán 3/4',	'Gabán largo y elegante',	105000.000,	140000.000,	'G-001',	'Paño',	'10-25',	'https://i.imgur.com/s6zyn2A.jpg',	'https://articulo.mercadolibre.com.co/MCO-569144190-1-gaban-34-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(10,	10,	'Conjunto Neru',	'Un conjunto formal para cualquier momento',	97500.000,	130000.000,	'Cj-001',	'Paño licrado',	'8-20',	'https://i.imgur.com/OqvR8l5.jpg',	'https://articulo.mercadolibre.com.co/MCO-569163477-1-y-3-conjunto-neru-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(21,	11,	'Bata escocés con flores',	'Vestido tipo chanel, cuello redondo, falda tipo lápiz, cremallera en la espalda, largo chanel.',	65000.000,	110000.000,	'B-952',	'Extra licra',	'Única (8-10)',	'https://i.imgur.com/XPpndel.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(8,	8,	'Chaqueta Corta',	'Chaquetas de calidad',	90000.000,	120000.000,	'Cc-001',	'Paño',	'8-20',	'https://i.imgur.com/WjHxF2a.jpg',	'https://articulo.mercadolibre.com.co/MCO-569148616-1-chaqueta-corta-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(5,	5,	'Pantalón Bota Recta',	'Pantalón de paño',	80000.000,	60000.000,	'Pbr-001',	'Paño licrado',	'6-22',	'https://i.imgur.com/s6zyn2A.jpg',	'https://articulo.mercadolibre.com.co/MCO-569139953-pantalon-bota-recta-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(9,	9,	'Blusa Bolero',	'Blusa con boleros a los lados de los  botones',	37500.000,	50000.000,	'Bb-001',	'Polyester y algodón',	'6-18',	'https://i.imgur.com/WjHxF2a.jpg',	'https://articulo.mercadolibre.com.co/MCO-569153060-2-blusa-bolero-_JM?quantity=1&variation=59362063173',	'1',	NULL,	NULL,	NULL,	NULL),
(18,	6,	'Gabán cuadros',	'Gaban cuadros grandes, corte princesa, cuatro botones, bolsillo lateral, forrado totalmente.',	80000.000,	120000.000,	'G-080',	'Paño piel de angel',	'8 y 10',	'https://i.imgur.com/hMfmYMY.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(7,	7,	'Enterizo Clásico Manga Larga',	'Enterizo elegante',	105000.000,	140000.000,	'Ent-001',	'Polyester y algodón',	'6-22',	'https://i.imgur.com/63DubOX.jpg',	'https://articulo.mercadolibre.com.co/MCO-569144506-enterizo-clasico-_JM?quantity=1',	'1',	NULL,	NULL,	NULL,	NULL),
(20,	6,	'Gabán sin botones',	'Gaban jaspeado, sin botones, ni cuello, totalmente forrado, con bolsillos laterales.',	90000.000,	120000.000,	'G-082',	'Paño liviano jaspeado',	'6-14',	'https://i.imgur.com/2Z1Wok3.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(14,	8,	'Chaqueta con Puños',	'Chaqueta corte princesa, con entalle en la espalda, forrada, con detalle en los puños.',	65000.000,	95000.000,	'Ch-065',	'Zubelda licrada',	'6-14',	'https://i.imgur.com/IlUucjx.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(22,	11,	'Bata flores chanel',	'Vestido escote en el pecho con herraje decorativo, largo hasta la rodilla, estampado en flores.',	65000.000,	110000.000,	'B-076',	'Extra licra',	'8-12',	'https://i.imgur.com/p7oPrnH.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(19,	6,	'Gabán cuadros pequeños',	'Gaban cuadros pequeños, corte princesa, totalmente forrado, cuello clásico .',	80000.000,	120000.000,	'Ch-003',	'Paño liviano',	'8-14',	'https://i.imgur.com/vOa0yGA.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(15,	12,	'Falda pretina alta',	'Falda pretina alta, cremallera en la parte posterior, detalle en la cintura para mejor realce.',	35000.000,	70000.000,	'F-70',	'Almara',	'8-16',	'https://i.imgur.com/3TN6Dts.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(16,	6,	'Gabán Capota',	'Gaban con capota removible, corte princesa con presilla en la espalda para acentuar cintura.',	90000.000,	120000.000,	'G-003',	'Paño Cachemir',	'8-16',	'https://i.imgur.com/PZCCxJt.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(17,	8,	'Chaqueta Puntas',	'Chaqueta en puntas, sin cuello,  con forro únicamente en la parte delantera.',	55000.000,	90000.000,	'Ch-069',	'Zubelda licrada',	'4-14',	'https://i.imgur.com/TtvnvyN.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(13,	6,	'Chaleco Gabán',	'Chaleco tipo gaban, detalle en la espalda, para mejor entalle y bolsillo lateral invisible.',	80000.000,	110000.000,	'G-002',	'Paño Elefant',	'6-14',	'https://i.imgur.com/FWSPrU6.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL),
(12,	11,	'Bata bolsillos',	'Bata cuello cuadrado, pretina a la altura del abdomen y cremallera en la espalda.',	60000.000,	110000.000,	'B060',	'Zubelda licrada',	'6-14',	'https://i.imgur.com/4WzysMW.jpg',	NULL,	'1',	NULL,	NULL,	NULL,	NULL);


DROP TABLE IF EXISTS "punto_venta";
DROP SEQUENCE IF EXISTS punto_venta_id_punto_venta_seq;
CREATE SEQUENCE punto_venta_id_punto_venta_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."punto_venta" (
    "id_punto_venta" integer DEFAULT nextval('punto_venta_id_punto_venta_seq') NOT NULL,
    "nombre_punto" character varying(100),
    "direccion" character varying(100),
    "telefono" numeric(11,0),
    "fecha_creacion" timestamp,
    "fecha_actualizacion" timestamp,
    CONSTRAINT "punto_venta_pkey" PRIMARY KEY ("id_punto_venta")
) WITH (oids = false);


DROP TABLE IF EXISTS "producto_ubicado";
CREATE TABLE "public"."producto_ubicado" (
    "id_producto" integer,
    "id_punto_venta" integer,
    CONSTRAINT "fk_productoubi_id_producto" FOREIGN KEY (id_producto) REFERENCES producto(id_producto) NOT DEFERRABLE,
    CONSTRAINT "fk_productoubi_id_punto_venta" FOREIGN KEY (id_punto_venta) REFERENCES punto_venta(id_punto_venta) NOT DEFERRABLE
) WITH (oids = false);



DROP TABLE IF EXISTS "ubicado";
CREATE TABLE "public"."ubicado" (
    "id_barrio" integer,
    "id_punto_venta" integer,
    CONSTRAINT "fk_ubicado_id_barrio" FOREIGN KEY (id_barrio) REFERENCES barrio(id_barrio) NOT DEFERRABLE,
    CONSTRAINT "fk_ubicado_id_punto_venta" FOREIGN KEY (id_punto_venta) REFERENCES punto_venta(id_punto_venta) NOT DEFERRABLE
) WITH (oids = false);


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "telefono" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "email_verified_at" timestamp(0),
    "password" character varying(255) NOT NULL,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);




DROP TABLE IF EXISTS "imagenes";
DROP SEQUENCE IF EXISTS imagenes_id_imagen_seq;
CREATE SEQUENCE imagenes_id_imagen_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 ;

CREATE TABLE "public"."imagenes" (
    "id_imagen" integer DEFAULT nextval('imagenes_id_imagen_seq') NOT NULL,
    "img" character varying(100),
    "id_producto" integer,
    CONSTRAINT "imagenes_pkey" PRIMARY KEY ("id_imagen"),
    CONSTRAINT "fk_producto_id_imagen" FOREIGN KEY (id_producto) REFERENCES producto(id_producto) NOT DEFERRABLE
) WITH (oids = false);

