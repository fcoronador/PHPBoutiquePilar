/** 
 * Author:Alvaro Felipe Coronado Romero
 * Date: 11/04/2020
 * Description: This is the creation shema for dbPilar, pleas execute in plsql console
 */
CREATE TABLE tipo_documento
(
	id_tipo_documento serial PRIMARY KEY,
	nombre_tipo_documento varchar(100)
);

CREATE TABLE persona
(
	id_persona serial PRIMARY KEY,
	id_barrio integer,
	id_tipo_documento integer,
	email varchar(150) ,
	numero_identificacion varchar(20),
	nombre varchar(100),
	estado varchar(1), --A activo -D desactivado
	rol varchar(3), -- ADM administrador,SNR sin rol 
  	fecha_creacion timestamp,
  	fecha_actualizacion timestamp 
);

CREATE TABLE ciudad
(
	id_ciudad serial PRIMARY KEY,
	nombre_ciudad varchar(50),
	fecha_creacion timestamp,
	fecha_actualizacion timestamp,
	creado_por varchar(50)
);

CREATE TABLE barrio
(
	id_barrio serial PRIMARY KEY,
	id_ciudad integer,
	nombre_barrio varchar(50),
	puntos_referencia varchar(150),
	fecha_creacion timestamp,
	fecha_actualizacion timestamp,
	creado_por varchar(50)
);

/* ---------------------------PRODUCTOS---------------------------------- */


CREATE TABLE categoria
(
	id_categoria serial PRIMARY KEY,
	nombre_categoria varchar(100),
	descripcion varchar(100),
    img varchar(100),
    visible boolean,
	fecha_creacion timestamp,
	fecha_actualizacion timestamp,
	creado_por varchar(50),
	editado_por varchar(50)
);


CREATE TABLE sub_categoria
(	
	id_subcategoria serial PRIMARY KEY,
	id_categoria integer,
	nombre_subcategoria varchar(50),
	descripcion varchar(100),
    img varchar(100),
    visible boolean,
	fecha_creacion timestamp,
	fecha_actualizacion timestamp,
	creado_por varchar(50),
	editado_por varchar(50)
);

CREATE TABLE producto
(
	id_producto serial PRIMARY KEY,
	id_subcategoria integer,
	nombre_producto varchar(100),
	descripcion varchar(2000),
	valor_unitario NUMERIC(13,3),
	precio_venta NUMERIC(13,3),
	referencia varchar(50),
	material varchar(50),
	medida varchar(50),
	img varchar(100),
    comprar varchar(300),
    visible boolean,
	fecha_creacion timestamp,
	fecha_actualizacion timestamp,
	creado_por varchar(50),
	editado_por varchar(50)
);


CREATE TABLE punto_venta
(
	id_punto_venta serial PRIMARY KEY,
	nombre_punto varchar(100),
	direccion varchar(100),
	telefono NUMERIC(11,0),
	fecha_creacion timestamp,
	fecha_actualizacion timestamp
);


CREATE TABLE ubicado
(
	id_barrio integer,
	id_punto_venta integer
);


CREATE TABLE producto_ubicado
(
	id_producto integer,
	id_punto_venta integer
);

CREATE TABLE imagenes
(
	id_imagen serial PRIMARY KEY,
    img2 varchar(100),
    img3 varchar(100),
    img4 varchar(100),
    img5 varchar(100),
    img6 varchar(100),
    img7 varchar(100),
    img8 varchar(100),
    id_producto integer
);

