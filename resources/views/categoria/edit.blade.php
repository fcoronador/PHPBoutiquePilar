@extends('plantilla')

@section('titulo','Editar|Categoría')
    

@section('contenido')
<div class="container py-3 shadow">
<form action="{{route('Updatecategoria',$id)}}" method="POST">
    @csrf
    <input type="hidden" name="_method" value="patch">
    <div class="form-group m-3">
        <input type="hidden" name="id" value="categoria">
        <label for="nombre_categoria">Nombre de la categoría</label>
        @foreach ($cat as $item)
        <input value="{{$item->nombre_categoria}}" type="text" name="nombre_categoria" id="" class="form-control" placeholder="">
        <span class="invalid-feeback text-danger" role="alert">
            {!! $errors->first('nombre_categoria','<small>:message</small><br>')!!}    
        </span>
        <label for="descripcion">Descripción</label>
        <input value="{{$item->descripcion}}" type="text" name="descripcion" id="" class="form-control" placeholder="">
        <span class="invalid-feeback text-danger" role="alert">
            {!! $errors->first('descripcion','<small>:message</small><br>')!!}    
        </span>
        <label for="img">Imagen</label>
        <input type="text" name="img" id="" class="form-control" placeholder="">
        <span class="invalid-feeback text-danger" role="alert">
            {!! $errors->first('img','<small>:message</small><br>')!!}
        </span>
        @endforeach
        <label for="visible">Publicar</label>
        <input type="checkbox" value="1" name="visible">
        <br>
        <input type="submit" value="Guardar" class="btn btn-primary btn-sm my-3">
    </div>
</form>
</div>

@endsection