@extends('plantilla')

@section('titulo','Editar | Subcategoría')

@section('contenido')
<div class="container py-3 shadow">
    
    <form action="{{route('Updatesubcategoria',$id)}}" method="post">

        

        @csrf
        <input type="hidden" name="_method" value="patch">
        <div class="form-group m-3 col-6">
            <label for="id_categoria">Categoría</label>
            <select class="form-control" name="id_categoria" id="">
                @foreach ($clist as $categoria)
                <option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
                @endforeach
            </select>
            <label for="nombre">Nombre de la Subcategoría</label>


            @foreach ($subcate as $item)
            <input value="{{$item->nombre_subcategoria}}" type="text" name="nombre_subcategoria" id=""
                class="form-control" placeholder="">
            <span class="invalid-feeback text-danger" role="alert">
                {!! $errors->first('nombre_subcategoria','<small>:message</small><br>')!!}
            </span>
            <label for="descripcion">Descripción</label>
            <input value="{{$item->descripcion}}" type="text" name="descripcion" id="" class="form-control"
                placeholder="">
            <span class="invalid-feeback text-danger" role="alert">
                {!! $errors->first('descripcion','<small>:message</small><br>')!!}
            </span>
            <label for="img">Imagen</label>
            <input type="text" name="img" id="" class="form-control" placeholder="">
            <span class="invalid-feeback text-danger" role="alert">
                {!! $errors->first('img','<small>:message</small><br>')!!}
            </span>
            @endforeach
            <label for="visible">Publicar</label>
            <input type="checkbox" value="1" name="visible">
            <br>
            <input class="btn btn-primary btn-sm  my-3" type="submit" value="Guardar">
        </div>
    </form>
</div>

@endsection