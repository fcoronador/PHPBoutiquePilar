@extends('plantilla')

@section('titulo','Administración')
    

@section('contenido')

<div class="col-12">
    @foreach ($prenda as $prop)
    
        <form action="{{route('Updateproducto',$prop->id_producto)}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="_method" value="patch">
            <div class="form-group m-3 col-6"> </div>
            <div class="form-group m-3 col-6">
                <input type="hidden" name="id_producto" value="{{$prop->id_producto}}">
                <input type="hidden" name="id_subcategoria" value="{{$prop->id_subcategoria}}">  
                <label for="nombre_producto">Nombre del Producto</label>
                <input type="text" name="nombre_producto" id="" class="form-control" placeholder="" value="{{$prop->nombre_producto}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('nombre_producto','<small>:message</small><br>')!!}    
                </span>

                <label for="descripcion">Descripción</label>
                <textarea class="form-control" 
                name="descripcion" id="" cols="30" rows="5">{{$prop->descripcion}}
                </textarea>
                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('descripcion','<small>:message</small><br>')!!}    
                </span>

                <label for="valor_unitario">Valor unitario</label>
                <input type="text" name="valor_unitario" id="" class="form-control" placeholder="" value="{{$prop->valor_unitario}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('valor_unitario','<small>:message</small><br>')!!}    
                </span>


                <label for="precio_venta">Precio Venta</label>
                <input type="text" name="precio_venta" id="" class="form-control" placeholder="" value="{{$prop->precio_venta}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('precio_venta','<small>:message</small><br>')!!}    
                </span>

                <label for="referencia">Referencia</label>
                <input type="text" name="referencia" id="" class="form-control" placeholder="" value="{{$prop->referencia}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('referencia','<small>:message</small><br>')!!}    
                </span>

                <label for="material">Material</label>
                <input type="text" name="material" id="" class="form-control" placeholder=""  value="{{$prop->material}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('material','<small>:message</small><br>')!!}    
                </span>

                <label for="Medida">Medida</label>
                <input type="text" name="medida" id="" class="form-control" placeholder="" 
                value="{{$prop->medida}}">

                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('medida','<small>:message</small><br>')!!}    
                </span>

                <label for="img">Imagen</label>
                    {{-- <input accept="image/*" type="file" name="img" id="" class="form-control" placeholder=""> --}}
                <input type="text" name="img" id="" class="form-control" placeholder="" 
                value="{{$prop->img}}">
                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('img','<small>:message</small><br>')!!}
                </span>

                <label for="comprar">Enlace de Mercadolibre</label>
                <input type="text" name="comprar" id="" class="form-control" placeholder="" 
                value="{{$prop->comprar}}">
                <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('comprar','<small>:message</small><br>')!!}
                </span>

                @endforeach
                <label for="visible">Publicar</label>
                <input type="checkbox" value="1" name="visible">
                <br>

                @foreach ($album as $imagen)
                    
                
                    <label for="img2">Imagen 2</label>
                    <input type="text" name="img2" id="" class="form-control" placeholder="" 
                    value="{{$imagen->img2}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img2','<small>:message</small><br>')!!}
                    </span>
                    <label for="img3">Imagen 3</label>
                    <input type="text" name="img3" id="" class="form-control" placeholder=""
                    value="{{$imagen->img3}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img3','<small>:message</small><br>')!!}
                    </span>
                    <label for="img4">Imagen 4</label>
                    <input type="text" name="img4" id="" class="form-control" placeholder=""
                    value="{{$imagen->img4}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img4','<small>:message</small><br>')!!}
                    </span>
                    <label for="img5">Imagen 5</label>
                    <input type="text" name="img5" id="" class="form-control" placeholder=""
                    value="{{$imagen->img5}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img5','<small>:message</small><br>')!!}
                    </span>
                    <label for="img6">Imagen 6</label>
                    <input type="text" name="img6" id="" class="form-control" placeholder=""
                    value="{{$imagen->img6}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img6','<small>:message</small><br>')!!}
                    </span>
                    <label for="img7">Imagen 7</label>
                    <input type="text" name="img7" id="" class="form-control" placeholder=""
                    value="{{$imagen->img7}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img7','<small>:message</small><br>')!!}
                    </span>
                    <label for="img8">Imagen 8</label>
                    <input type="text" name="img8" id="" class="form-control" placeholder=""
                    value="{{$imagen->img8}}">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img8','<small>:message</small><br>')!!}
                    </span>
                @endforeach
                <input class="btn btn-primary btn-sm float-right my-3" type="submit" value="Guardar">
            </div>
        </form>
    
</div>




@endsection