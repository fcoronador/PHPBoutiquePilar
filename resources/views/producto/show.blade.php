@extends('plantilla')

@section('titulo','Producto')


@section('contenido')

<div class="container ">
    @forelse ($prenda as $item)
    @if ($item->visible)
    <h1>{{$item->nombre_producto}}</h1>
    <hr>
    <div class="container d-flex justify-content-center">
        <div class="row">
            <div class="py-3 col-12  col-lg-6 shadow">
                <picture>
                    <source srcset="{{$item->img}}" type="image/svg+xml">
                    <img src="{{$item->img}}" class="img-fluid " alt="...">
                </picture>
            </div>

            <div class="col-12 col-lg-6 shadow">

                <div class="row flex-row-reverse">
                    <a class="float-right" href="https://www.facebook.com/profile.php?id=100053095032091">
                        <img src="/img/03.png" alt="" srcset="">
                    </a>
                    <a class="float-right" href="https://www.instagram.com/koronastore8/?hl=es-la">
                        <img src="/img/04.png" alt="" srcset="">
                    </a>
                    <a class="float-right" href="https://twitter.com/?lang=es">
                        <img src="/img/05.png" alt="" srcset="">
                    </a>
                </div>

                <hr>
                <span>Nombre :</span>
                <br>
                <span class="float-right">{{$item->nombre_producto}}</span>
                <br>
                <span>Descripción :</span>
                <br>
                <span class="float-right">{{$item->descripcion}}</span>
                <br>
                <span>Color :</span>
                <br>
                <span class="float-right">Surtidos</span>
                <br>
                <span>Referencia:</span>
                <br>
                <span class="float-right">{{$item->referencia}}</span>
                <br>
                <span>Tallas:</span>
                <br>
                <span class="float-right">{{$item->medida}}</span>
                <br>
                <span>Precio:</span>
                <br>
                <span class="float-right">{{round($item->precio_venta)}} pesos </span>
                <br>
                <div class="alert alert-info my-2">
                    
                        <h5>Sobre los Precios</h5>
                        El botón comprar te llevará a nuestra tienda de Mercadolibre,
                        el precio aumentará por costos de comisión y envío, si deseas
                        conservar el precio de tienda contactanos al siguiente número:
                        <br>
                        <ion-icon name="logo-whatsapp"></ion-icon> 311 297 27 48
                        <br>
                        <ion-icon name="mail-outline"></ion-icon>
                        b.femenina.pilar@gmail.com
                    
                </div>
                <a href="{{$item->comprar}}"> <button class="btn btn-primary btn-lg my-2">Comprar</button></a>
            </div>
            @endif
            @empty
            <span>No hay producto</span>
            @endforelse
        </div>
    </div>
</div>

@endsection