@extends('plantilla')

@section('titulo','Quienes somos')



@section('contenido')
<div class="container">
    <h1 class="display-4 text-primary">Nosotros Somos!!</h1>
    <hr>
    <div class="container">
        <h1>Misión</h1>
        <hr>
        <p>
            Comercializar ropa formal y productos de alta calidad,
            cumpliendo con los precios del mercado,
            satisfaciendo a los clientes con diseño y estilo.
        </p>
    </div>
    <div class="container">
        <h1>Visión</h1>
        <hr>
        <p>
            Para el año 2020 convertirnos en una de las empresas más fuertes de Bogot&aacute;,
            siempre cumpliendo con los acuerdos que regulan nuestro negocio y sus alcances.
        </p>
    </div>
    <div class="container">
        <h1>Valores</h1>
        <hr>
        <ul>
            <li>
                Dar atención amable a nuestros clientes considerando las necesidades que quiere sastifacer con nuestros
                productos .
            </li>
            <li>
                Ofrecer precios justos y productos de calidad.
            </li>
            <li>
                Compromiso con el bienestar de nuestros clientes internos.
            </li>
        </ul>

    </div>
</div>
@endsection