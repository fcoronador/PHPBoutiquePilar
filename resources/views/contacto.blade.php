@extends('plantilla')

@section('titulo','Contactenos')
    

@section('contenido')
    
<div class="row">
    <div class="col-12 col-sm-10 col-lg-6 mx-auto">
        <div class="container">
            <form 
                class="bg-white shadow rounded py-3 px-4"
                action="{{route('contacto')}}" method="POST">
                {{ csrf_field() }}
                <h1 class="display-4 text-primary">{{ __('Contact') }}</h1>
                <hr>
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input id="name" 
                    class="form-control shadow-sm   
                        @if($errors->first('name'))  
                            is-invalid 
                        @else
                            border-0
                        @endif"
                     value="{{old('name')}}" type="text" name='name' placeholder='Nombre...' >
        
                        <span class="invalid-feeback text-danger" role="alert">
                            {!! $errors->first('name','<small>:message</small><br>')!!}    
                        </span>
                     
                    <label for="email">Correo</label>
                    <input id="email"
                    class="form-control bg-light shadow-sm  
                        @if($errors->first('email'))  
                            is-invalid 
                        @else
                            border-0
                        @endif"
                    value="{{old('email')}}" type="text" name='email' placeholder='Correo...'>
                    
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('email','<small>:message</small><br>')!!}
                    </span>
                
                    <label for="asunto">Asunto</label>
                    <input id="asunto"
                    class="form-control bg-light shadow-sm  
                        @if($errors->first('asunto'))  
                            is-invalid 
                        @else
                            border-0
                        @endif"
                    value="{{old('asunto')}}" type="subject" name='asunto' placeholder='Asunto...'>
                    
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('asunto','<small>:message</small><br>')!!}
                    </span>
        
                    <label for="content">Mensaje</label>
                    <textarea 
                    class="form-control bg-light shadow-sm  
                        @if($errors->first('content'))  
                            is-invalid 
                        @else
                            border-0
                        @endif"
                    name="content" id="" cols="30" rows="5" placeholder="Mensaje...">{{old('content')}}</textarea>
        
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('content','<small>:message</small><br>')!!}
                    </span>
        
                    <input class="btn btn-primary btn-lg btn-block" type="submit" value="Enviar !!!">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection