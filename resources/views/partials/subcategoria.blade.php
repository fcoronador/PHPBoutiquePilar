<a class="back-to-gallery button-link button-link-icon" href="{{route('inicio')}}">
    <span class="novi-icon icon icon-primary">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
    </span>
    <span>Regresar</span>
</a>
<div class="tab-pane fade show active" id="tabs-1-1">
    <div class="tabs-custom tabs-horizontal tabs-gallery hide-on-modal" id="tabs-galery">

        <ul class="nav nav-tabs">
            @forelse ($slist as $item)
            @if($item->visible)
            @if ($id==$item->id_categoria)

            <li class="nav-item" >
                <a class="nav-link" href="{{route('subcatalogo', $item->nombre_subcategoria)}}">
                <img src="{{$item->img}}" alt="" width="180" height="180" />
                <span>&nbsp;</span>
                <span class="text-dark">{{$item->nombre_subcategoria}}</span>
                </a>
            </li>
            @endif
            @endif
            @empty
            No hay subcategorias
            @endforelse
        </ul>
    </div>
</div>