<div class="tab-pane fade" id="tabs-1-4">
    <div class="content-box hide-on-modal">
      <div class="row row-30 justify-content-center align-items-center">
        {{-- <div class="col-12 col-md-7">
          <!--RD Mailform-->

        </div> --}}
        <div class="col-12 col-md-6">
          <h3>Información de Contacto</h3>
          <h3>Pilar Romero</h3>
          <h4>Asesora Comercial</h4>
          <address class="subtitle">{{-- Dirección --}}<br>Bogotá D.C, Colombia.</address>
          <h4>
          <ul class="contact-info">
            <li>
                <i class="fa fa-whatsapp" aria-hidden="true"></i>
                <span>Whatsapp:</span><a href="tel:#">311 297 27 48 </a></li>
            <li>
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>Teléfono:</span><a href="">311 297 27 48 </a></li>
            {{-- <li><span>FAX:</span><a href="tel:#">+1 800 889 9898</a></li> --}}
            <li>
                <i class="fa fa-envelope-open" aria-hidden="true"></i>
                <span>E-mail:</span><a href="">b.femenina.pilar@gmail.com</a></li>
          </ul>
        </h4>
        </div>
      </div><a class="close-content-box" href="#">x</a>
    </div>
  </div>