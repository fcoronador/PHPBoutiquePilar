<div class="tab-pane fade show active" id="tabs-1-1">
    <div class="tabs-custom tabs-horizontal tabs-gallery hide-on-modal" id="tabs-galery">

        <!-- Nav tabs-->
        <ul class="nav nav-tabs">

            @forelse ($clist as $categoria)
              @if ($categoria->visible)
                <li class="nav-item" >
                <a class="nav-link" href="{{route('nuevosubcat',$categoria->id_categoria)}}" >
                        <img src="{{$categoria->img}}" alt="" width="180" height="180" />
                        <span>&nbsp;</span>
                        <span class="text-dark">{{$categoria->nombre_categoria}}</span>
                    </a>
                </li>
              @endif
            @empty
            No hay categorías
            @endforelse
        </ul>


    </div>
</div>