<header class="section page-header">
    <div class="container">
        <div class="row justify-content-between align-items-end row-30">
        <div class="col-12 col-md-6"><a class="brand-logo" href="{{route('inicio')}}">
                    <img src="/img/logo.png" alt="" width="346" height="62" /></a></div>
            <div class="col-12 col-md-6 col-xl-4">
                <div class="head-title">
                    <p>Una vitrina Colombiana <br class="d-none d-md-inline-block">a tu servicio.
                    </p><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</header>