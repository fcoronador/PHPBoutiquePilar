<!DOCTYPE html>
<html class="wide" lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Fredy Coronado">
    <meta name="description" content="Esta una vitrina para diferentes productos">
    <meta name="keywords" content="koronastore, koronadostore.com, ropa, batas, gabanes, productos, mujer, mujeres, ropa formal, tendencia, primavera, domicilio, dotaciones, chaquetas, blusas, tops, barato, economico, fabrica, clothes, jackets, camisas, women, woman, wears">
    <meta name="robots" content="index,follow">
    <!-- Site Title-->
    <title>@yield('titulo')</title>
    {{-- Meta --}}
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport"
        content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/style.css">
    

    {{-- <script src="/js/app.js" defer></script> --}}

    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <link rel="manifest" href="/img/site.webmanifest">
    <!-- Stylesheets-->
    <style>
        .ie-panel {
            display: none;
            background: #212121;
            padding: 10px 0;
            box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
            clear: both;
            text-align: center;
            position: relative;
            z-index: 1;
        }

        html.ie-10 .ie-panel,
        html.lt-ie-10 .ie-panel {
            display: block;
        }
    </style>
</head>

<body>
    <div class="ie-panel">
        <a href="http://windows.microsoft.com/en-US/internet-explorer/">
            <img src="/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
        </a>
    </div>
    <div class="page bg-image novi-background">
        @yield('contenido')

    </div>

    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="/js/core.min.js"></script>
    <script src="/js/script.js"></script>
    <script src="https://kit.fontawesome.com/8cfc6cecc2.js" crossorigin="anonymous"></script>
    <!-- coded by ragnar-->
</body>
</html>