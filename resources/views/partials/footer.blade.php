<footer class="section footer-classic context-dark novi-background">
    <div class="container">
      <div class="footer-group">
        <ul class="list-inline list-inline-xs">
          <li><a class="fa fa-facebook icon" href="#"></a></li>
          <li><a class="fa fa-twitter icon" href="#"></a></li>
          <li><a class="fa fa-google-plus icon" href="#"></a></li>
          <li><a class="fa fa-vimeo icon" href="#"></a></li>
          <li><a class="fa fa-youtube icon" href="#"></a></li>
          <li><a class="fa fa-pinterest-p icon" href="#"></a></li>
        </ul>
        
        <!-- Rights-->
        <p class="rights">
        <span>&copy;&nbsp;</span>
        <span class="copyright-year"></span>
        <span>&nbsp;</span>
        <span>.&nbsp;</span>
        <a href="#" data-toggle="modal" data-target="#privacy">Privacy Policy</a>. Design&nbsp;by&nbsp;
        <a href="https://www.templatemonster.com/">Templatemonster</a>
        <span>&nbsp;</span>
        <span>.&nbsp;</span>
        <a href="http://www.freepik.com">Background designed by Freepik</a>
        </p>      
      </div>
    </div>
    <!-- Coded by JeremyLuis-->
</footer>