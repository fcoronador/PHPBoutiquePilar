
<div class="tab-pane fade show active" id="tabs-1-1">
    <div class="tabs-custom tabs-horizontal tabs-gallery hide-on-modal" id="tabs-galery">
        
        <ul class="nav nav-tabs">

            @forelse ($subcat as $item)
            @if ($item->visible)
            <li class="nav-item" role="presentation">
              <a class="nav-link" href="#tabs-gallery-{{$item->id_producto}}" data-toggle="tab">
                <img src="{{$item->img}}" alt="" width="180" height="180"/>
              <span>&nbsp;</span>
              <span class="text-dark">{{$item->nombre_producto}} <br> ${{round($item->precio_venta)}} </span>
              </a>
            </li>
            @endif
            @empty
            <span>No hay articulos en esta categoria</span>
            @endforelse
        </ul>

        <!-- Tab panes-->
        <div class="tab-content">
          @forelse ($subcat as $item)
          @if ($item->visible)
            <div class="tab-pane fade" id="tabs-gallery-{{$item->id_producto}}">
            <div class="gallery-wrap">
              <div class="gallery-wrap-inner">
                <h4>{{$item->nombre_producto}}</h4>
                <div class="dots-custom-{{$item->id_producto}}"></div>

                <a class="back-to-gallery button-link button-link-icon" href="#">
                    <span class="novi-icon icon icon-primary">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                    </span>
                    <span>Regresar</span>
                </a>
                <a href="{{$item->comprar}}"> 
                    <button class="btn btn-primary btn-lg my-2 float-right">Comprar</button>
                </a>

                </div>

              <div class="gallery-wrap-inner">
                <!-- Owl Carousel-->
                <div class="owl-carousel owl-gallery" data-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="false" data-margin="0" data-mouse-drag="false" data-lightgallery="group" data-pagination-class=".dots-custom-{{$item->id_producto}}">

                <a class="gallery-item" href="{{$item->img}}" data-lightgallery="item">
                    <figure>
                        <div class="d-md-flex d-lg-flex d-xl-flex col-md-12 col-lg-12 col-xl-12">
                            
                                <div class="d-sm-block col-md-6 col-lg-6 col-xl-6">
                                    <picture>
                                        <source srcset="{{$item->img}}" type="image/svg+xml">
                                        <img src="{{$item->img}}" class="img-fluid" alt="...">
                                    </picture>
                                </div>
                                <div class="d-sm-block col-md-6 col-lg-6 col-xl-6 card m-1">
                                    <div class="card-body">
                                        <table class="table">
                                            <tr>
                                                <th class="text-center" colspan="2">{{$item->nombre_producto}}</th>
                                            </tr>
                                            <tr>
                                                <td>Precio: </td>
                                                <td>$ {{round($item->precio_venta)}} pesos</td>
                                            </tr>
                                            <tr>
                                                <td>Colores: </td>
                                                <td>Surtidos</td>
                                            </tr>
                                            <tr>
                                                <td>Referencia: </td>
                                                <td>{{$item->referencia}}</td>
                                            </tr>
                                            <tr>
                                                <td>Tallas: </td>
                                                <td>{{$item->medida}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Descripción: </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{{$item->descripcion}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                         
                        </div>
                    </figure>
                    <div class="caption"><span class="fa fa-expand"></span></div>
                </a>
                
                


                @include('partials.album')
                
                
                

                </div>

                

              </div>
            </div>
            </div>
          @endif
          @empty
          <span>No hay articulos en esta categoria</span>
          @endforelse
        </div>

    </div>
</div>

    