<div class="tab-pane fade" id="tabs-1-2">
    <div class="content-box hide-on-modal">
      <div class="content-box-inner">
        <div class="row align-items-center row-30 row-md-30">
          <div class="col-12 col-md-6 col-lg-7">
            <figure><img src="/img/04.svg" alt="" width="504" height="369"/>
            </figure>
          </div>
          <div class="col-12 col-md-6 col-lg-5">
            <h3>Quiénes somos nosotros?</h3>
            <p class="subtitle">Korona Store</p>
            <p>Korona Store es una vitrina donde encontrará un amplio catálogo de ropa formal, accesorios y zapatos, aquí publicamos nuestra amplia gama de productos a los que puedes acceder y consultar todos los detalles.
            </p><a class="button button-primary button-icon button-icon-left" href="#" data-toggle="modal" data-target="#modal-about-us">
                <span>Nuestros Principios</span>
                <span>
                    &nbsp;&nbsp;
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </span> 
            </a>
          </div>
        </div>
      </div><a class="close-content-box" href="#">x</a>
    </div>

    <div class="modal slideUp" id="modal-about-us" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="content-box">
            <div class="content-box-inner">
              <div class="row">
                <div class="col-12">
                  <h3>Misión</h3>
                  <p>
                    Comercializar ropa formal y productos de alta calidad,
                    cumpliendo con los precios del mercado,
                    satisfaciendo a los clientes con diseño y estilo.
                  </p>
                  <h3>Visión</h3>
                  <p>
                    Para el año 2020 convertirnos en una de las empresas más fuertes de Bogot&aacute;,
                    siempre cumpliendo con los acuerdos que regulan nuestro negocio y sus alcances.
                  </p>
                  <h3>Valores</h3>
                  <p>
                    <ul>
                        <li>
                            Dar atención amable a nuestros clientes considerando las necesidades que quiere satisfacer con nuestros
                            productos .
                        </li>
                        <li>
                            Ofrecer precios justos y productos de calidad.
                        </li>
                        <li>
                            Compromiso con el bienestar de nuestros clientes internos.
                        </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div><a class="close-modal-content-box" href="#" data-dismiss="modal">x</a>
          </div>
        </div>
      </div>
    </div>
  </div>