<!-- Nav tabs-->
<ul class="nav nav-tabs">
    <li class="nav-item" role="presentation"><a class="nav-link active" href="#tabs-1-1" data-toggle="tab"><span
                class="nav-link-main">Catálogo</span></a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-1-2" data-toggle="tab"><span
                class="nav-link-main">Acerca de nosotros</span></a></li>
    <li class="nav-item" role="presentation"><a class="nav-link" href="#tabs-1-3" data-toggle="tab"><span
                class="nav-link-main">Servicios</span></a></li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" href="#tabs-1-4" data-toggle="tab">
        <span class="nav-link-main">Contacto</span>
        </a>
    </li>

    @guest
    <li class="nav-item" >
        <a class="nav-link" href="{{ route('login') }}">
            <span class="nav-link-main">Entrar</span>
        </a>
    </li>
    @else
    <li class="nav-item" >
        <a class="nav-link" href="{{route('admin')}}">
            <span class="nav-link-main">{{ Auth::user()->name }}</span>
        </a>
    </li>
    @endguest

</ul>