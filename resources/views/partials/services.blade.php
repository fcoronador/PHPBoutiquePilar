<div class="tab-pane fade" id="tabs-1-3">
    <div class="content-box hide-on-modal">
      <div class="content-box-inner row-30">
        <h3>Servicios</h3>
        <div class="row align-items-center row-30">
          <div class="col-12 col-md-6">
            <div class="unit service-box">
              <div class="unit-left">
                  <i class="fa fa-truck" aria-hidden="true" style="font-size: 4vw"></i>
                </div>
              <div class="unit-body"><a class="subtitle" href="#" data-toggle="modal" data-target="#modal-service-1">Atención a Domicilio o Empresa</a>
                <p>Nosotros prestamos el servicio a domicilio o empresa para entrega de productos y asesoría. En la ciudad de Bogotá. </p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="unit service-box">
              <div class="unit-left">
                  <i class="fa fa-person-booth" aria-hidden="true" style="font-size: 4vw"></i>
                </div>
              <div class="unit-body"><a class="subtitle" href="#" data-toggle="modal" data-target="#modal-service-2">Dotaciones</a>
                <p>Nosotros producimos dotaciones para trabajos formales tipo blusa, pantalón y blazer e informales tipo camiseta (Polo ó T-shirt) &nbsp;y bluejean.</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="unit service-box">
              <div class="unit-left">
                  <i class="fas fa-chess-board" style="font-size: 4vw"></i>
                </div>
              <div class="unit-body"><a class="subtitle" href="#" data-toggle="modal" data-target="#modal-service-3">Bordados</a>
                <p>Nosotros prestamos el servicio de bordado para las prendas que deseas comprar aplican condiciones para el cálculo del precio.</p>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="unit service-box">
              <div class="unit-left">
                  <i class="fas fa-stamp" style="font-size: 4vw"></i>
                </div>
              <div class="unit-body"><a class="subtitle" href="#" data-toggle="modal" data-target="#modal-service-4">Estampados</a>
                <p>Nosotros prestamos el servicio de estampado para las prendas que deseas comprar aplican condiciones para el cálculo del precio.</p>
              </div>
            </div>
          </div>
        </div>
      </div><a class="close-content-box" href="#">x</a>
    </div>
    <div class="modal slideUp" id="modal-service-1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="content-box">
            <div class="content-box-inner">
              <div class="row">
                <div class="col-12">
                  <h3>Atención a Domicilio o Empresa</h3>
                  <p>Nosotros prestamos el servicio a domicilio o empresa para entrega de productos, realizar toma de medidas, selección de productos, color y telas.</p>
                  <p>Para tomar este servicio se recomienda:
                    <br>  
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Ubicarse en Bogotá.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Seleccionar al menos 5 productos que quiera ver en fisico.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Tener claro la cantidad de productos que va adquirir.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Comunicarse con nuestra Asesora Pilar Romero: <i class="fa fa-phone" aria-hidden="true"></i> 311 297 27 48.                       
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-leaf" aria-hidden="true"></i> Por nuestra política ambiental ayudanos a mantener la cantidad de viajes a la menor cantidad posible.                     
                  </p>
                  <p> Por fuera de Bogotá:
                    <br> 
                    
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Solo ofrece servicio de entrega por fuera de Bogotá y el costo del envió se negocia <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;con nuestra  Asesora Pilar Romero: <i class="fa fa-phone" aria-hidden="true"></i> 311 297 27 48.
                  </p>
                </div>
              </div>
            </div><a class="close-modal-content-box" href="#" data-dismiss="modal">x</a>
          </div>
        </div>
      </div>
    </div>
    <div class="modal slideUp" id="modal-service-2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="content-box">
            <div class="content-box-inner">
              <div class="row">
                <div class="col-12">
                  <h3>Dotaciones</h3>
                  <p>Nosotros producimos dotaciones para oficina y trabajos en que la presentación de los trabajadores es formal como secretariado, atención al cliente, servicios públicos, educación, transporte por contrato ó vigilancia privada. También  proveemos dotaciones informales tipo camiseta (Polo ó T-shirt) y bluejean.</p>
                  <p>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> El servicio de dotación también se puede prestar con el servicio a domicilio o empresa.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Se pueden añadir prendas a la dotación como: camisas, chaquetas reflexivas y corbatas.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Se pueden añadir bordados o estampados a las prendas.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Mas información comunicarse con nuestra Asesora Pilar Romero: <i class="fa fa-phone" aria-hidden="true"></i> 311 297 27 48.
                  </p>
                </div>
              </div>
            </div><a class="close-modal-content-box" href="#" data-dismiss="modal">x</a>
          </div>
        </div>
      </div>
    </div>
    <div class="modal slideUp" id="modal-service-3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="content-box">
            <div class="content-box-inner">
              <div class="row">
                <div class="col-12">
                  <h3>Bordados</h3>
                  <p>Nosotros bordamos el logo de tu empresa o marca en las prendas que estas adquiriendo con nosotros con un costo adicional.</p>
                  <p>Debes tener en cuenta que los siguientes aspectos afectan el precio de un bordado: 
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Tamaño.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Colores.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Precisión del dibujo bordado.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Cantidad de bordados a realizar.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Cantidad de hilos usados.
                    <br>

                  </p>
                </div>
              </div>
            </div><a class="close-modal-content-box" href="#" data-dismiss="modal">x</a>
          </div>
        </div>
      </div>
    </div>
    <div class="modal slideUp" id="modal-service-4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="content-box">
            <div class="content-box-inner">
              <div class="row">
                <div class="col-12">
                  <h3>Estampados</h3>
                  <p>Nosotros prestamos el servicio de estampado para las prendas que deseas comprar aplican condiciones para el cálculo del precio. No recomendamos estampar prendas en paño, blusas  ó camisas.</p>
                  <p>Debes tener en cuenta que los siguientes aspectos afectan el precio de un estampado: 
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Tamaño.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Colores.
                  </p>
                </div>
              </div>
            </div><a class="close-modal-content-box" href="#" data-dismiss="modal">x</a>
          </div>
        </div>
      </div>
    </div>
  </div>