<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje enviado desde la web</title>
</head>
<body>
    <h1>Contenido de un correo electronico</h1>
    <hr>
    <p>
       Recibiste un mensaje de: {{$mensaje['name']}},
       {{$mensaje['email']}}
    </p>
    <p>Asunto: {{$mensaje['asunto']}}</p>
    <p>Contenido: {{$mensaje['content']}}</p>

</body>
</html>