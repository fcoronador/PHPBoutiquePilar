@extends('partials.plantilla')

@section('titulo','Producto')
    
@section('contenido')

    @include('partials.preload')
    @include('partials.header')
    <section class="section section-xs">
      <div class="container">
        {{-- Boton de regreso --}}
        <a class="back-to-gallery button-link button-link-icon" href="{{route('inicio')}}">
            <span class="novi-icon icon icon-primary">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </span> 
            <span>Inicio</span>
        </a>
        {{-- Alerta de precios --}}
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <strong>Sobre los Precios: </strong> 
            El botón comprar te llevará a nuestra tienda de Mercadolibre, el precio aumentará por costos de comisión y envío, si deseas conservar el precio de tienda contactanos al siguiente número: <i class="fa fa-whatsapp" aria-hidden="true"></i> 311 297 27 48 <i class="fa fa-envelope" aria-hidden="true"></i> b.femenina.pilar@gmail.com
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

          <div class="tabs-custom tabs-horizontal tabs-corporate" id="tabs-1">
            @include('partials.navTabs')
            <div class="tab-content"> 
              @include('partials.product')
              @include('partials.about')
              @include('partials.services')
              @include('partials.contact')
            </div>
          </div>
      </div>
    </section>
    
    @include('partials.privacy')
    @include('partials.footer')
    
@endsection