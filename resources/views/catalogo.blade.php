@extends('plantilla')

@section('titulo','Catálogo')


@section('contenido')


<div class="container ">
    <h1 class="display-4 text-primary">{{$id}}</h1>
    <hr>
    <div class="container d-flex">
        <div class="row">

            @forelse ($subcat as $item)
            @if ($item->visible)

            <div class="py-3 col-12  col-lg-3 shadow">
                <picture>
                    <source srcset="{{$item->img}}" type="image/svg+xml">
                    <a href="{{route('productos', $item->referencia)}}"><img src="{{$item->img}}" class="img-fluid " alt="..."></a>
                </picture>
            </div>
            @endif
            @empty
            <span>No hay articulos en esta categoria</span>
            @endforelse
        </div>
    </div>
</div>

@endsection