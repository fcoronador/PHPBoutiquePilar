     
<div class="container ">
    <nav class="navbar navbar-expand-lg navbar-light shadow " >
            <a class="navbar-brand" href="{{route('inicio')}}"><img src="/img/favicon-32x32.png" alt="" srcset=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item"><a class="nav-link" href="{{route('inicio')}}">Inicio</a></li>
            <li class="nav-item"><a class="nav-link dropdown-toggle" role="button" data-toggle="collapse" data-target="#catalogos1" aria-controls="catalogos1" aria-expanded="true" aria-label="Toggle navigation">Catálogo</a></li>
          
            <li class="nav-item"><a class="nav-link" href="">Quiénes Somos?</a></li>
            <li class="nav-item"><a class="nav-link" href="">Contáctenos</a></li>
               
        </ul>
        
        <ul class="navbar-nav">
            @guest
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @else
             <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                    <a href="{{route('admin')}}" class="dropdown-item">Administrar</a>
                    <a href="{{route('register')}}" class="dropdown-item">Registrar Administrador</a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                 </div>
                </li>
            @endguest
        </ul>
    </div>
    </nav>
    <div class="collapse shadow" id="catalogos1">
        <div class="bg-light p-4">
            @forelse ($clist as $categoria)
            <div class="btn-group">
    
                @if ($categoria->visible)   
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$categoria->nombre_categoria}}
                    </button>
                @endif
                <div class="dropdown-menu">
                   @forelse ($slist as $item)
                   @if($item->visible)
                        @if ($categoria->id_categoria===$item->id_categoria)
                        <a class="dropdown-item" href="{{route('subcatalogo', $item->nombre_subcategoria)}}">
                            {{$item->nombre_subcategoria}}</a>
                        @endif
                    @endif
                   @empty
                       no hay items
                   @endforelse
                </div>
            </div>
           @empty
               <li>No hay catálogos</li>
           @endforelse


        </div>
    </div>
</div>