@extends('partials.plantilla')

@section('titulo','Subcategoria')
    
@section('contenido')

    @include('partials.preload')
    @include('partials.header')
    <section class="section section-xs">
      <div class="container">
          <div class="tabs-custom tabs-horizontal tabs-corporate" id="tabs-1">
            @include('partials.navTabs')
            <div class="tab-content"> 
              @include('partials.subcategoria')
              @include('partials.about')
              @include('partials.services')
              @include('partials.contact')
             

            </div>
          </div>
      </div>
    </section>
    
    @include('partials.privacy')
    @include('partials.footer')
    
@endsection