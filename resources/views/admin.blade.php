@extends('partials.plantillaAdmin')

@section('titulo','Administración')
    

@section('contenido')


<div class="d-flex container" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <div class="sidebar-heading">Menú</div>
      <div class="list-group list-group-flush">
        <a id="ItemCate" href="#" class="list-group-item list-group-item-action bg-light">Categorias</a>
        <a id="ItemSub" href="#" class="list-group-item list-group-item-action bg-light">Subcategorias</a>
        <a id="ItemProd" href="#" class="list-group-item list-group-item-action bg-light">Productos</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">
            <i class="fa fa-building" aria-hidden="true"></i>
        </button>
        
            <a class="nav-item ">
            <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} 
            </a>
            <a href="{{route('Vercatalogo')}}" class=" nav-item text-decoration-none text-dark">
                Ver Catálogo
            </a>
        
                <div class="dropdown-menu dropdown-menu-top" aria-labelledby="navbarDropdown">
        
                    <a href="{{route('admin')}}" class="dropdown-item">Administrar</a>
                    <a href="{{route('register')}}" class="dropdown-item">Registrar Administrador</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </a>
        
      </nav>

    <div id="paneCate" class="container show">
        <h1 class="mt-4"><ion-icon name="layers-outline"></ion-icon>Categorias</h1>
        <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#contentId" aria-expanded="false" aria-controls="contentId">
                Crear Categoría
            </a>
        </p>
        <div class="collapse" id="contentId">
            <form action="{{route('Guardarcatalogo')}}" method="POST">
                @csrf
                <div class="form-group m-3">
                    <input type="hidden" name="id" value="categoria">
                    <label for="nombre_categoria">Nombre de la categoría</label>
                    <input type="text" name="nombre_categoria" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('nombre_categoria','<small>:message</small><br>')!!}
                    </span>
                    <label for="descripcion">Descripción</label>
                    <input type="text" name="descripcion" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('descripcion','<small>:message</small><br>')!!}
                    </span>
                    <label for="img">Imagen</label>
                    <input type="text" name="img" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img','<small>:message</small><br>')!!}
                    </span>
                    <label for="visible">Publicar</label>
                    <input type="checkbox" value="1" name="visible" checked>
                    <input type="submit" value="Guardar" class="btn btn-primary btn-sm float-right my-3">
                </div>
            </form>
        </div>
    </div>

    <div id="paneSub" class="container fade">
        <h1 class="mt-4"><ion-icon name="add-circle-outline"></ion-icon>Subcategorias</h1>
        <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#contentId2" aria-expanded="false" aria-controls="contentId2">
                Crear Subcategoría
            </a>
        </p>
        <div class="collapse" id="contentId2">
            <form action="{{route('Guardarcatalogo')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="subcategoria">
                <div class="form-group m-3 col-6">

                </div>
                <div class="form-group m-3 col-lg-6">
                    <label for="id_categoria">Categoría</label>
                    <select class="form-control" name="id_categoria" id="">
                        @foreach ($categorias as $categoria)
                        <option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
                        @endforeach
                    </select>
                    <label for="nombre">Nombre de la Subcategoría</label>
                    <input type="text" name="nombre_subcategoria" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('nombre_subcategoria','<small>:message</small><br>')!!}
                    </span>
                    <label for="descripcion">Descripción</label>
                    <input type="text" name="descripcion" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('descripcion','<small>:message</small><br>')!!}
                    </span>
                    <label for="img">Imagen</label>
                    <input type="text" name="img" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img','<small>:message</small><br>')!!}
                    </span>
                    <label for="visible">Publicar</label>
                    <input type="checkbox" value="1" name="visible" checked>
                    <input class="btn btn-primary btn-sm float-right my-3" type="submit" value="Guardar">
                </div>
            </form>
    </div>

    </div>
      <div id="paneProd" class="container fade">
        <h1 class="mt-4"><ion-icon name="shirt-outline"></ion-icon>Productos</h1>
        <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#contentId3" aria-expanded="false" aria-controls="contentId3">
                Crear Productos
            </a>
        </p>
        <div class="collapse" id="contentId3">
            <form action="{{route('Guardarcatalogo')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="producto">
                <div class="form-group m-3 col-6">

                </div>
                <div class="form-group m-3 col-12">
                <div class="d-flex">

                   <div class="col-6">

                   
                    <label for="id_categoria">Categoría</label>
                    <select class="form-control" name="id_categoria" id="">
                        @foreach ($categorias as $categoria)
                        <option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
                        @endforeach
                    </select>
                    <label for="id_subcategoria">Subcategoría</label>
                    <select class="form-control" name="id_subcategoria" id="">
                        @foreach ($categorias as $categoria)
                        @foreach($categoria->subcat as $sub)
                        <option value="{{$sub->id_subcategoria}}">{{$sub->nombre_subcategoria}}</option>
                        @endforeach
                        @endforeach
                    </select>
                    <label for="nombre">Nombre del Producto</label>
                    <input type="text" name="nombre_producto" id="" class="form-control" placeholder="">

                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('nombre_producto','<small>:message</small><br>')!!}
                    </span>

                    <label for="descripcion">Descripción</label>
                    <textarea class="form-control" name="descripcion" id="" cols="30" rows="5"></textarea>
                    <span class="invalid-feeback text-danger" role="alert">
                    {!! $errors->first('descripcion','<small>:message</small><br>')!!}    
                    </span>

                    <label for="valor_unitario">Valor unitario</label>
                    <input type="text" name="valor_unitario" id="" class="form-control" placeholder="">

                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('valor_unitario','<small>:message</small><br>')!!}
                    </span>

                    <label for="precio_venta">Precio Venta</label>
                    <input type="text" name="precio_venta" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('precio_venta','<small>:message</small><br>')!!}
                    </span>
                </div>

                <div class="col-6">

                    <label for="referencia">Referencia</label>
                    <input type="text" name="referencia" id="" class="form-control" placeholder="">

                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('referencia','<small>:message</small><br>')!!}
                    </span>

                    <label for="material">Material</label>
                    <input type="text" name="material" id="" class="form-control" placeholder="">

                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('material','<small>:message</small><br>')!!}
                    </span>

                    <label for="Medida">Medida</label>
                    <input type="text" name="medida" id="" class="form-control" placeholder="">

                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('medida','<small>:message</small><br>')!!}
                    </span>

                    <label for="img">Imagen</label>
                    {{-- <input accept="image/*" type="file" name="img" id="" class="form-control" placeholder=""> --}}
                    <input type="text" name="img" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img','<small>:message</small><br>')!!}
                    </span>
                    <label for="comprar">Enlace de Mercadolibre</label>
                    <input type="text" name="comprar" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('comprar','<small>:message</small><br>')!!}
                    </span>
                    <label for="visible">Publicar</label>
                    <input type="checkbox" value="1" name="visible" checked>
                    <br>
                    <label for="img2">Imagen 2</label>
                    <input type="text" name="img2" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img2','<small>:message</small><br>')!!}
                    </span>
                    <label for="img3">Imagen 3</label>
                    <input type="text" name="img3" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img3','<small>:message</small><br>')!!}
                    </span>
                    <label for="img4">Imagen 4</label>
                    <input type="text" name="img4" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img4','<small>:message</small><br>')!!}
                    </span>
                    <label for="img5">Imagen 5</label>
                    <input type="text" name="img5" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img5','<small>:message</small><br>')!!}
                    </span>
                    <label for="img6">Imagen 6</label>
                    <input type="text" name="img6" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img6','<small>:message</small><br>')!!}
                    </span>
                    <label for="img7">Imagen 7</label>
                    <input type="text" name="img7" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img7','<small>:message</small><br>')!!}
                    </span>
                    <label for="img8">Imagen 8</label>
                    <input type="text" name="img8" id="" class="form-control" placeholder="">
                    <span class="invalid-feeback text-danger" role="alert">
                        {!! $errors->first('img8','<small>:message</small><br>')!!}
                    </span>


                    <input class="btn btn-primary btn-sm float-right my-3" type="submit" value="Guardar">
                </div>
                
                </div>
                </div>
            </form>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
  <!-- /#wrapper -->


@endsection