@extends('partials.plantillaAdmin')

@section('titulo','Administración')


@section('contenido')

<div class="container text-dark">
    <div class="row">
        <div class="card py-4 col-12  col-lg-4 shadow">
            <div class="btn-group-vertical">

                @foreach ($categorias as $categoria)
                <div class="btn-group" role="group">
                    <a class="btn btn-light border" data-toggle="collapse"
                        data-target="#{{$categoria->nombre_categoria}}" aria-controls="{{$categoria->nombre_categoria}}"
                        aria-expanded="true" aria-label="Toggle navigation">{{$categoria->nombre_categoria}}</a>

                    <a class="btn btn-primary pl-1 col-1"
                        href="{{route('Actualizarcategoria',$categoria->id_categoria)}}">
                        <ion-icon class="" name="create-outline"></ion-icon>
                    </a>

                    <a class="btn btn-danger pl-1 col-1"
                        onclick="document.getElementById('deleteC{{$categoria->id_categoria}}').submit()">
                        <ion-icon name="trash-outline"></ion-icon>
                    </a>
                    <form class="d-none" id="deleteC{{$categoria->id_categoria}}"
                        action="{{route('Borrarcategoria',$categoria->id_categoria)}}" method="post">
                        {{ csrf_field() }} {{method_field('patch')}}
                        <input type="hidden" name="visibile" value="0">
                    </form>
                </div>
                @endforeach

            </div>
        </div>

        <div class="card py-4 col-12  col-lg-4  shadow">

            @foreach ($categorias as $item)
            <div class="collapse btn-group-vertical container-fluid" id="{{$item->nombre_categoria}}">

                @foreach($item->subcat as $i)

                <div class="btn-group " role="group">
                    <a class="btn btn-light border " data-toggle="collapse" data-target="#{{$i->nombre_subcategoria}}"
                        aria-controls="{{$i->nombre_subcategoria}}" aria-expanded="true" aria-label="Toggle navigation">
                        {{$i->nombre_subcategoria}}</a>

                    <a class="btn btn-primary pl-1 col-1"
                        href="{{route('Actualizarsubcategoria',$i->id_subcategoria)}}">
                        <ion-icon name="create-outline"></ion-icon>
                    </a>

                    <a class="btn btn-danger pl-1 col-1"
                        onclick="document.getElementById('deleteS{{$i->id_subcategoria}}').submit()">
                        <ion-icon name="trash-outline"></ion-icon>
                    </a>
                    <form class="d-none" id="deleteS{{$i->id_subcategoria}}"
                        action="{{route('Borrarsubcategoria',$i->id_subcategoria)}}" method="post">
                        {{ csrf_field() }} {{method_field('patch')}}
                        <input type="hidden" name="visibile" value="0">
                    </form>
                </div>

                @endforeach
            </div>
            @endforeach


        </div>

        <div class="card py-4 col-12  col-lg-4 shadow"> {{-- Producto --}}

            @foreach ($categorias as $catego)
            @foreach ($catego->subcat as $subcate)

            <div class="collapse btn-group-vertical container-fluid" id="{{$subcate->nombre_subcategoria}}">

                @foreach($subcate->prod as $i)
                <div class="btn-group " role="group">

                    <a class="btn btn-light border" data-toggle="list3" href="#" role="tab3">
                        {{$i->nombre_producto}}
                    </a>

                    <a class="btn btn-primary pl-1 col-1" href="{{route('Actualizarproducto', $i->id_producto)}}">
                        <ion-icon name="create-outline"></ion-icon>
                    </a>

                    <a class="btn btn-danger pl-1 col-1"
                        onclick="document.getElementById('deleteP{{$i->id_producto}}').submit()">
                        <ion-icon name="trash-outline"></ion-icon>
                    </a>
                    <form class="d-none" id="deleteP{{$i->id_producto}}"
                        action="{{route('Borrarproducto',$i->id_producto)}}" method="post">
                        {{ csrf_field() }} {{method_field('patch')}}
                        <input type="hidden" name="visibile" value="0">
                    </form>
                </div>

                @endforeach

            </div>

            @endforeach
            @endforeach

        </div>
    </div>
</div>


@endsection