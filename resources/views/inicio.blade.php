@extends('plantilla')

@section('titulo','Inicio')


@section('contenido')


<div class="container d-flex">
    <div class="row">
        <div class="col-12 col-lg-6">
            <h1 class="display-4 text-black">Korona Store</h1>
            Bienvenido @auth {{auth()->user()->name}} @endauth
            <p class="lead text-secondary">
                <i class="fa fa-heartbeat" aria-hidden="true"></i>
                A Korona Store donde encontrará un amplio catálogo de ropa formal, accesorios y zapatos
                , aquí publicamos nuestra amplia gama de productos a los que puedes acceder y
                consultar todos los detalles.
                <br>
                Si estas interesado o interasada en alguno de nuestros productos 100% nacionales e importados de alta calidad, puedes usar el botón comprar que te
                redirigira a nuestra tienda en Mercadolibre, garantizando satisfación y seguridad en la compra.
                <br>
                También ofrecemos servicios adicionales como: dotaciones, uniformes, bordados y estampados
                personalizados que se pueden cotizar mediante nuestro número de contacto en whatsapp:
                <br> 
                <ion-icon name="logo-whatsapp"></ion-icon> 311 297 27 48 
                <br>
                <ion-icon name="mail-outline"></ion-icon> 
                 b.femenina.pilar@gmail.com
            </p>

            <a class="btn btn-lg btn-block btn-primary" href="{{route('contacto')}}">Contáctame</a>

        </div>
        <div class="py-3 col-12  col-lg-6">
            <picture>
                <source srcset="/img/01.svg" type="image/svg+xml">
                <img src="/img/01.svg" class="img-fluid " alt="...">
            </picture>
        </div>
    </div>
</div>

@endsection